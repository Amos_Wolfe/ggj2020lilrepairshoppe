﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

[CustomEditor(typeof(Item))]
public class ItemEditor : Editor
{
    private const float CONNECTION_CHECK_RADIUS = 0.04f;
    private const string ATTACHMENT_POINT_TAG = "AttachmentPoint";

    private Collider[] colliderList;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        GUILayout.Space(20);

        if(GUILayout.Button("Refresh Attachment Points"))
        {
            RefreshAttachmentPoints();
        }
    }

    private void RefreshAttachmentPoints()
    {
        Item item = target as Item;
        Scene itemScene = item.gameObject.scene;
        PhysicsScene physicsScene = itemScene.GetPhysicsScene();
        if (colliderList == null)
            colliderList = new Collider[100];

        SerializedProperty attachArrayProp = serializedObject.FindProperty("configuredAttachmentPoints");

        for(int i = 0; i < attachArrayProp.arraySize; i++)
        {
            SerializedProperty attachProp = attachArrayProp.GetArrayElementAtIndex(i);
            AttachmentPoint attachment = attachProp.objectReferenceValue as AttachmentPoint;
            GameObject.DestroyImmediate(attachment.gameObject);
        }
        attachArrayProp.arraySize = 0;

        List<Transform> attachConfigList = GetAttachmentPointConfigurationZones();

        for (int i = 0; i < attachConfigList.Count; i++)
        {
            var attachConfig = attachConfigList[i];

            List<Segment> segmentsFound = new List<Segment>();
            int collisionCount = physicsScene.OverlapSphere(attachConfig.position, CONNECTION_CHECK_RADIUS, colliderList, -1, QueryTriggerInteraction.Ignore);
            for (int a = 0; a < collisionCount; a++)
            {
                Segment colliderSegment = colliderList[a].GetComponentInParent<Segment>();
                if (colliderSegment != null && !segmentsFound.Contains(colliderSegment))
                    segmentsFound.Add(colliderSegment);
            }

            if (segmentsFound.Count != 2)
            {
                Debug.LogErrorFormat("{0} segments found when initialising attachment point '{1}'. There should be 2 segments found at each attachment point.", segmentsFound.Count, attachConfig.name);
                continue;
            }

            CreateAttachments(attachConfig, segmentsFound[0], segmentsFound[1], attachArrayProp);

            attachConfig.gameObject.SetActive(false);
        }

        serializedObject.ApplyModifiedProperties();
        EditorUtility.SetDirty(item);
    }

    private List<Transform> GetAttachmentPointConfigurationZones()
    {
        Item item = target as Item;

        List<Transform> attachConfigList = new List<Transform>();
        foreach (Transform child in item.transform)
        {
            if (child.tag == ATTACHMENT_POINT_TAG)
                attachConfigList.Add(child);
        }

        return attachConfigList;
    }

    private void CreateAttachments(Transform config, Segment firstSegment, Segment secondSegment, SerializedProperty attachArrayProp)
    {
        if (firstSegment == secondSegment)
        {
            Debug.LogErrorFormat("Attachment point {0} is trying to be configured within just one segment (the attachment point needs to be placed between 2 segments).", config);
            return;
        }

        AttachmentPoint attachmentPointTemplate = serializedObject.FindProperty("attachmentPointTemplate").objectReferenceValue as AttachmentPoint;

        float attachmentPointScale = serializedObject.FindProperty("attachmentPointScale").floatValue;

        AttachmentPoint firstPoint = (AttachmentPoint)PrefabUtility.InstantiatePrefab(attachmentPointTemplate, firstSegment.transform);
        firstPoint.transform.SetPositionAndRotation(config.position, config.rotation);
        AttachmentPoint secondPoint = (AttachmentPoint)PrefabUtility.InstantiatePrefab(attachmentPointTemplate, secondSegment.transform);
        secondPoint.transform.SetPositionAndRotation(config.position, config.rotation);

        attachArrayProp.arraySize += 2;
        attachArrayProp.GetArrayElementAtIndex(attachArrayProp.arraySize - 2).objectReferenceValue = firstPoint;
        attachArrayProp.GetArrayElementAtIndex(attachArrayProp.arraySize - 1).objectReferenceValue = secondPoint;

        firstPoint.transform.localScale = Vector3.one * attachmentPointScale;
        secondPoint.transform.localScale = Vector3.one * attachmentPointScale;

        SerializedObject firstPointSO = new SerializedObject(firstPoint);
        firstPointSO.FindProperty("correctAttachment").objectReferenceValue = secondPoint;
        firstPointSO.ApplyModifiedProperties();
        SerializedObject secondPointSO = new SerializedObject(secondPoint);
        secondPointSO.FindProperty("correctAttachment").objectReferenceValue = firstPoint;
        secondPointSO.ApplyModifiedProperties();

        Vector3 firstDirection = firstSegment.transform.position - config.position;
        firstDirection.Normalize();
        float flipDot = Vector3.Dot(config.forward, firstDirection);
        if (flipDot > 0)
            firstPoint.transform.Rotate(180, 0, 0);
        else
            secondPoint.transform.Rotate(180, 0, 0);
    }
}
