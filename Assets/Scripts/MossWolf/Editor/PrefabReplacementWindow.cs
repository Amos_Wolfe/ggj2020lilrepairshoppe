﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

public class PrefabReplacementWindow : EditorWindow
{
    [MenuItem("Tools/Prefab Replacement")]
    public static void CreateWindow()
    {
        var newWindow = GetWindow<PrefabReplacementWindow>();
        newWindow.Show();
    }

    GameObject currentPrefab;
    private float replaceChance = 1;

    private void OnGUI()
    {
        currentPrefab = (GameObject) EditorGUILayout.ObjectField("Replace With", currentPrefab, typeof(GameObject), false);

        replaceChance = EditorGUILayout.FloatField("Replace Chance", replaceChance);

        if (GUILayout.Button("Replace"))
            ReplacePrefabs();
    }

    private void ReplacePrefabs()
    {
        if (currentPrefab == null)
            return;

        GameObject[] selectionList = Selection.gameObjects;

        if (!EditorUtility.DisplayDialog("Replace Prefabs?", string.Format("Would you like to replace {0} Gameobjects with the {1} prefab?", selectionList.Length, currentPrefab.name), "Yes", "No"))
            return;

        List<GameObject> newSelection = new List<GameObject>(selectionList);

        for(int i = 0; i < selectionList.Length; i++)
        {
            GameObject selectedObject = selectionList[i];
            GameObject newObject = (GameObject)PrefabUtility.InstantiatePrefab(currentPrefab);

            if (replaceChance < 1 && UnityEngine.Random.value > replaceChance)
                continue;

            newSelection.Add(newObject);
            newObject.transform.SetParent(selectedObject.transform.parent, true);
            newObject.transform.SetSiblingIndex(selectedObject.transform.GetSiblingIndex());
            newObject.transform.localPosition = selectedObject.transform.localPosition;
            newObject.transform.localRotation = selectedObject.transform.localRotation;
            newObject.transform.localScale = selectedObject.transform.localScale;

            newSelection.Remove(selectedObject);

            EditorSceneManager.MarkSceneDirty(newObject.scene);

            GameObject.DestroyImmediate(selectedObject, true);
        }

        Selection.objects = newSelection.ToArray();
    }
}
