﻿using MossWolf.Utility.GameObjectUtil;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace MossWolf.Editor
{

	public static class ApplyAllPrefabChangesCommand
	{
		private static readonly System.Type[] IGNORE_TYPE_ARRAY = { typeof(RectTransform), typeof(Transform) };

		[MenuItem("Tools/Commands/Apply All Prefab Changes")]
		static void ApplyChanges()
		{
			var gameObjectList = GameObjectUtils.FindAllGameObjects();

			for (int i = 0; i < gameObjectList.Count; i++)
			{
				var currentObject = gameObjectList[i];
				if (currentObject == null) continue;

				GameObject prefab = (GameObject)PrefabUtility.GetCorrespondingObjectFromSource(currentObject);
				GameObject prefabRoot = PrefabUtility.FindPrefabRoot(prefab);
				if (prefab != null && prefab == prefabRoot)
				{
					var modifications = PrefabUtility.GetPropertyModifications(currentObject);
					if (modifications == null) continue;

					bool changeDetected = false;
					foreach (var mod in modifications)
					{
						if (mod.target != null && !IsIgnored(mod.target.GetType()))// && !string.IsNullOrEmpty(mod.value))
						{
							if (!string.IsNullOrEmpty(mod.value))
							{
								changeDetected = true;
								//Debug.LogFormat("Change detected in {0}: {1}. Value: {2}", currentObject.name, mod.target.GetType(), mod.value);
							}
							else if (mod.objectReference != null)
							{
								GameObject objRef = null;
								if (mod.objectReference is GameObject)
									objRef = (GameObject)mod.objectReference;
								else if (mod.objectReference is Component)
									objRef = ((Component)mod.objectReference).gameObject;

								if (objRef != null)
								{
									if (GameObjectUtils.ObjectExistsInParent(objRef.transform, currentObject.transform))
									{
										//Debug.Log(currentObject.name + " " + mod.objectReference.name);
										changeDetected = true;
									}
								}
								else
								{
									//Debug.Log(currentObject.name + " " + mod.objectReference.name + " " + mod.objectReference.GetType());
									changeDetected = true;
								}
							}

						}

					}

					if (changeDetected)
					{
						Debug.LogFormat("Applied changes to prefab {0}", prefab.name);
						PrefabUtility.ReplacePrefab(currentObject, prefab, ReplacePrefabOptions.ConnectToPrefab);
					}
					//Debug.LogFormat("Object {0} has changes, with prefab {1}", currentObject.name, prefab.name);
					//PrefabUtility.ReplacePrefab(currentObject, prefab, ReplacePrefabOptions.ConnectToPrefab);
				}
			}
		}

		private static bool IsIgnored(System.Type type)
		{
			bool ignored = false;

			for (int i = 0; i < IGNORE_TYPE_ARRAY.Length; i++)
			{
				if (IGNORE_TYPE_ARRAY[i] == type)
					return true;
			}

			return ignored;
		}
	}

}