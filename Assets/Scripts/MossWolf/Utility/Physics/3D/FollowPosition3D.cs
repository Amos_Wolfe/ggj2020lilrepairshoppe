﻿using MossWolf.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace MossWolf.Utility.Physics
{
    /// <summary>
    /// Follows a target object on the late update
    /// </summary>
    public class FollowPosition3D : BaseBehaviour
    {
        [SerializeField]
        private Transform target;
        [SerializeField]
        private float followAmount = 1;
        [SerializeField]
        private bool followX, followY, followZ;

        private Transform thisTransform;

        protected override void AwakeEx()
        {
            base.AwakeEx();

            thisTransform = transform;
        }

        private void LateUpdate()
        {
            Vector3 targetPosition = target.position;

            Vector3 thisPosition = thisTransform.position;

            Vector3 offset = targetPosition - thisPosition;
            offset *= Mathf.Min(Time.deltaTime * followAmount, 1);
            if (!followX)
                offset.x = 0;
            if (!followY)
                offset.y = 0;
            if (!followZ)
                offset.z = 0;

            thisPosition += offset;

            thisTransform.position = thisPosition;
        }
    }
}