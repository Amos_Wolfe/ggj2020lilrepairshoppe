﻿using MossWolf.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace MossWolf.Utility.Physics
{
    /// <summary>
    /// Follows a target object rotation on the late update
    /// </summary>
    public class FollowRotation3D : BaseBehaviour
    {
        [SerializeField]
        private Transform target;
        [SerializeField]
        private float followAmount = 90;
        [SerializeField]
        private bool followX, followY, followZ;

        private Transform thisTransform;

        protected override void AwakeEx()
        {
            base.AwakeEx();

            thisTransform = transform;
        }

        private void LateUpdate()
        {
            thisTransform.rotation = Quaternion.Slerp(thisTransform.rotation, target.rotation, Time.deltaTime * followAmount);
        }
    }
}