﻿using MossWolf.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MossWolf.Utility.Physics
{
    /// <summary>
    /// Controls for moving a player with a rigid body using the 3D physics system
    /// </summary>
    public class CharacterRigidbody3D : BaseBehaviour
    {
        [SerializeField]
        private string movingAnimationKey = "Running";
        [SerializeField]
        private string horizontalAxisName = "Horizontal";
        [SerializeField]
        private string verticalAxisName = "Vertical";

        [SerializeField]
        private float movementSpeed = 1;
        [SerializeField]
        private float turningSpeed = 1;

        private Animator thisAnim;
        private Rigidbody thisBody;
        
        protected override void AwakeEx()
        {
            base.AwakeEx();

            GetComponentEx(ref thisAnim, ref thisBody);
        }

        private void Update()
        {
            float horizontal = Input.GetAxis(horizontalAxisName);
            float vertical = Input.GetAxis(verticalAxisName);

            thisBody.AddRelativeForce(0, 0, vertical * movementSpeed, ForceMode.Acceleration);
            thisBody.AddRelativeTorque(0, horizontal * turningSpeed, 0, ForceMode.Acceleration);

            bool isMoving = vertical != 0;
            thisAnim.SetBool(movingAnimationKey, isMoving);
        }
    }
}