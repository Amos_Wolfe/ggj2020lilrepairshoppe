﻿using MossWolf.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Runs the UnityEvent when Physics.CheckBox returns true.
/// </summary>
public class OnIntersect2D : BaseBehaviour
{
	public enum Shape
	{
		Rectangle,
		Circle,
	}

	[SerializeField]
	private LayerMask collisionLayers;
	[SerializeField]
	private Shape shape = Shape.Rectangle;
	[SerializeField]
	private UnityEvent onIntersect;

	private void OnDrawGizmosSelected()
	{
		switch(shape)
		{
			case Shape.Rectangle:
				Vector3 scale = transform.localScale;
				scale.z = 0;
				Gizmos.DrawWireCube(transform.position, scale);
				break;
			case Shape.Circle:
				Gizmos.DrawWireSphere(transform.position, transform.localScale.x);
				break;
		}
	}

	private void FixedUpdate()
	{
		bool collisionFound = false;

		switch(shape)
		{
			case Shape.Rectangle:
				collisionFound = Physics2D.OverlapBox(transform.position, transform.localScale, 0, collisionLayers) != null;
				break;
			case Shape.Circle:
				collisionFound = Physics2D.OverlapCircle(transform.position, transform.localScale.x, collisionLayers) != null;
				break;
		}

		if (collisionFound)
			onIntersect.Invoke();
	}
}
