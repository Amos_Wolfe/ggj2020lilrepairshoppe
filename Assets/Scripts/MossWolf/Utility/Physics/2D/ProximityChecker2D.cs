﻿using MossWolf.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MossWolf.Utility.Physics
{
	/// <summary>
	/// Sends out raycasts in a circle.
	/// </summary>
	public class ProximityChecker2D : BaseBehaviour
	{
		public enum PhysicsMode { Physics2D, Physics3D};

		[SerializeField]
		private PhysicsMode physicsMode;
		[SerializeField]
		private float range = 5;
		[SerializeField]
		private float castThickness = 0.5f;
		[SerializeField, Tooltip("The amount of checks to do in a circle")]
		private int checks = 32;
		[SerializeField]
		private LayerMask collisionLayers;

		public float Closeness { get; set; }

		private void FixedUpdate()
		{
			float closestDistance = float.MaxValue;

			float interval = 360.0f / checks;
			Vector3 forward = Vector3.right;

			Vector2 thisPos = transform.position;

			for (int i = 0; i < checks; i++)
			{
				switch (physicsMode)
				{
					case PhysicsMode.Physics2D:
						var hit2D = UnityEngine.Physics2D.CircleCast(thisPos, castThickness, (Vector2)forward, range, collisionLayers);
						if (hit2D.transform != null)
						{
							float distance = Vector2.Distance(thisPos, hit2D.point);
							closestDistance = Mathf.Min(closestDistance, distance);
						}
						break;
					case PhysicsMode.Physics3D:
						RaycastHit hit3D;
						if (UnityEngine.Physics.SphereCast(thisPos, castThickness, forward, out hit3D, range, collisionLayers))
						{
							float distance = Vector2.Distance(thisPos, hit3D.point);
							closestDistance = Mathf.Min(closestDistance, distance);
						}
						break;
				}

				forward = Quaternion.Euler(0, 0, interval) * forward;
			}

			Closeness = Mathf.Clamp01((range - closestDistance) / range);
		}
	}
}