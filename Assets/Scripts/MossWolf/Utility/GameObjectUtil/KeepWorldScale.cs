﻿using MossWolf.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MossWolf.Utility.GameObjectUtil
{
	/// <summary>
	/// Makes this object stay at scale 1,1,1 in world scale, regardless of its parents scale (provided no parent is at scale 0)
	/// </summary>
	public class KeepWorldScale : BaseBehaviour
	{
		private void LateUpdate()
		{
			if (transform.parent == null)
			{
				transform.localScale = Vector3.one;
				return;
			}

			Vector3 lossyScale = transform.parent.localScale;

			Vector3 counterScale = Vector3.zero;
			counterScale.x = 1.0f / Mathf.Max(0.0001f, lossyScale.x);
			counterScale.y = 1.0f / Mathf.Max(0.0001f, lossyScale.y);
			counterScale.z = 1.0f / Mathf.Max(0.0001f, lossyScale.z);

			transform.localScale = counterScale;
		}
	}
}