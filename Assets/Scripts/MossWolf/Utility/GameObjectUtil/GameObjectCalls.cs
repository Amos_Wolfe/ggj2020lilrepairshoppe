﻿using MossWolf.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MossWolf.Utility.GameObjectUtil
{
	/// <summary>
	/// Extra methods that can be used by UnityEvents in the inspector.
	/// </summary>
	public class GameObjectCalls : BaseBehaviour
	{
		public void DestroyThisObject()
		{
			DestroyObject(gameObject);
		}

		public void SendToPosition(Transform position)
		{
			if (position == null) return;
			transform.position = position.position;
		}
	}
}