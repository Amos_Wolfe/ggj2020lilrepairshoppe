﻿using MossWolf.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MossWolf.Utility.GameObjectUtil
{
	/// <summary>
	/// Sets the target gameobject to active or inactive when the target key is pressed
	/// </summary>
	public class ToggleGameObjectOnKeyPress : BaseBehaviour
	{
		private enum KeyEventType { KeyDown, KeyUp, KeyHeld };

		[SerializeField, Tooltip("The key to press to activate/deactivate the Target Object below")]
		private KeyCode keyCode;

		[SerializeField, Tooltip("Set to key down to toggle the Target Object as soon as the key is pressed.\nSet to key up to toggle the Target Object when the key is released.\nSet to key held to toggle the Target Object every frame that the key is held")]
		private KeyEventType eventType = KeyEventType.KeyDown;

		[SerializeField, Tooltip("The object to activate/deactivate")]
		private GameObject targetObject;

		private void Update()
		{
			switch (eventType)
			{
				case KeyEventType.KeyDown:
					UpdateKeyDown();
					break;
				case KeyEventType.KeyUp:
					UpdateKeyUp();
					break;
				case KeyEventType.KeyHeld:
					UpdateKeyHeld();
					break;
			}
		}

		private void UpdateKeyDown()
		{
			if (Input.GetKeyDown(keyCode))
				ToggleGameObject();
		}

		private void UpdateKeyUp()
		{
			if (Input.GetKeyUp(keyCode))
				ToggleGameObject();
		}

		private void UpdateKeyHeld()
		{
			if (Input.GetKey(keyCode))
				ToggleGameObject();
		}

		private void ToggleGameObject()
		{
			if (targetObject != null)
				targetObject.SetActive(!targetObject.activeSelf);
		}
	}
}