﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MossWolf.Utility.GameObjectUtil
{
	public static class GameObjectUtils
	{
		public static List<T> FindAll<T>() where T : Component
		{
			var activeScene = SceneManager.GetActiveScene();

			List<T> componentList = new List<T>();

			var rootObjectList = activeScene.GetRootGameObjects();
			for (int i = 0; i < rootObjectList.Length; i++)
			{
				var currentObject = rootObjectList[i];
				if (currentObject == null) continue;

				componentList.AddRange(currentObject.GetComponentsInChildren<T>(true));
			}

			return componentList;
		}

		public static List<GameObject> FindAllGameObjects()
		{
			var activeScene = SceneManager.GetActiveScene();

			List<GameObject> gameObjectList = new List<GameObject>();

			var rootObjectList = activeScene.GetRootGameObjects();
			for (int i = 0; i < rootObjectList.Length; i++)
			{
				FindAllGameObjects(gameObjectList, rootObjectList[i]);
			}

			return gameObjectList;
		}

		public static bool ObjectExistsInParent(Transform targetObject, Transform parent)
		{
			if (targetObject == null) return false;
			if (targetObject == parent) return true;
			return ObjectExistsInParent(targetObject.transform.parent, parent);
		}

		private static void FindAllGameObjects(List<GameObject> gameObjectList, GameObject currentObject)
		{
			if (currentObject == null) return;
			gameObjectList.Add(currentObject);

			foreach (Transform child in currentObject.transform)
			{
				FindAllGameObjects(gameObjectList, child.gameObject);
			}
		}

        public static string GetObjectPath(GameObject targetObject)
        {
            return GetObjectPath(targetObject, targetObject.name);
        }

        private static string GetObjectPath(GameObject targetObject, string currentPath)
        {
            var parent = targetObject.transform.parent;
            if(parent != null)
            {
                currentPath = string.Format("{0}/{1}", parent.name, currentPath);
                return GetObjectPath(parent.gameObject, currentPath);
            }

            return currentPath;
        }
	}
}