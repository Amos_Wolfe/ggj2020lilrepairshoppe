﻿using MossWolf.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MossWolf.Utility.GameObjectUtil
{
	/// <summary>
	/// Makes UI elements vertically stay in the center of the screen.
	/// </summary>
	public class StayCenterOfScreen : BaseBehaviour
	{
		private RectTransform thisRect;

		protected override void AwakeEx()
		{
			base.AwakeEx();

			GetComponentEx(ref thisRect);
		}

		private void LateUpdate()
		{
			Vector2 screenMiddle = new Vector2(Screen.width / 2, Screen.height / 2);

			Vector2 localPoint;
			RectTransformUtility.ScreenPointToLocalPointInRectangle(thisRect, screenMiddle, null, out localPoint);

			transform.localPosition = transform.localPosition + Vector3.up * localPoint.y;
		}
	}
}