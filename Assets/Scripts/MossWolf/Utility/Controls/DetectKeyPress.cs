﻿using MossWolf.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace MossWolf.Utility.Controls
{
	/// <summary>
	/// Calls the Unity Event when a key is pressed on the keyboard
	/// </summary>
	public class DetectKeyPress : BaseBehaviour
	{
		private enum KeyEventType { KeyDown, KeyUp, KeyHeld };

		[SerializeField, Tooltip("The key to press to call the Unity Event below")]
		private KeyCode keyCode;

		[SerializeField, Tooltip("Set to key down to call the event as soon as the key is pressed.\nSet to key up to call the event when the key is released.\nSet to key held to call the unity event every frame that the key is held")]
		private KeyEventType eventType = KeyEventType.KeyDown;

		[SerializeField, Tooltip("What to do when the key is pressed.")]
		private UnityEvent unityEvent;

		private void Update()
		{
			switch (eventType)
			{
				case KeyEventType.KeyDown:
					UpdateKeyDown();
					break;
				case KeyEventType.KeyUp:
					UpdateKeyUp();
					break;
				case KeyEventType.KeyHeld:
					UpdateKeyHeld();
					break;
			}
		}

		private void UpdateKeyDown()
		{
			if (Input.GetKeyDown(keyCode))
				unityEvent.Invoke();
		}

		private void UpdateKeyUp()
		{
			if (Input.GetKeyUp(keyCode))
				unityEvent.Invoke();
		}

		private void UpdateKeyHeld()
		{
			if (Input.GetKey(keyCode))
				unityEvent.Invoke();
		}
	}
}