﻿using MossWolf.Base;
using MossWolf.Utility.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MossWolf.Utility.Controls
{
    /// <summary>
    /// Panning and zoom controls for the camera
    /// </summary>
    public class Rts2dCameraController : BaseBehaviour
    {
        private const float MOVE_WITH_SCROLL_VALUE = 0.00185f;
        private const float MAX_DELTA_TIME = 0.1f;

        [SerializeField]
        private float moveSpeed = 1.5f;
        [SerializeField]
        private float zoomSpeed = 10;
        [SerializeField]
        private float speedUpMultiplier = 3;
        [SerializeField]
        private Vector2 zoomBounds = new Vector2(3, 50);
        [SerializeField]
        private float interpolationSpeed = 15;

        [SerializeField]
        private string horizontalAxis = "Horizontal";
        [SerializeField]
        private string verticalAxis = "Vertical";
        [SerializeField]
        private string speedUpButton = "Sprint";
        [SerializeField]
        private string zoomAxis = "Mouse ScrollWheel";

        private Camera thisCamera;
        private float currentZoom;
        private float targetZoom;
        private Vector3 targetPosition;

        /// <summary>
        /// Used to change the zoom of this camera using another script, without interferring with the controls.
        /// </summary>
        public float ZoomMultiplier { get; set; }

        /// <summary>
        /// If set to false this controller will not change the camera's position
        /// </summary>
        public bool PositionEnabled { get; set; }

        protected override void AwakeEx()
        {
            base.AwakeEx();

            GetComponentEx(ref thisCamera);
            currentZoom = thisCamera.orthographicSize;
            targetZoom = thisCamera.orthographicSize;

            ZoomMultiplier = 1;
            PositionEnabled = true;

            ResetPosition();
        }

        private void Update()
        {
            float unscaledDeltaTime = Mathf.Min(Time.unscaledDeltaTime, MAX_DELTA_TIME);

            var horizontal = Input.GetAxisRaw(horizontalAxis) * moveSpeed * unscaledDeltaTime;
            var vertical = Input.GetAxisRaw(verticalAxis) * moveSpeed * unscaledDeltaTime;
            var scroll = Input.GetAxisRaw(zoomAxis) * zoomSpeed;

            if (UIUtils.IsMouseOverUI())
                scroll = 0;

            horizontal *= thisCamera.orthographicSize;
            vertical *= thisCamera.orthographicSize;

            if(Input.GetButton(speedUpButton))
            {
                horizontal *= speedUpMultiplier;
                vertical *= speedUpMultiplier;
            }

            targetPosition.x += horizontal;
            targetPosition.y += vertical;

            float previousZoom = targetZoom;
            targetZoom = Mathf.Clamp(targetZoom - scroll, zoomBounds.x, zoomBounds.y);
            float zoomChange = targetZoom - previousZoom;
            if (zoomChange != 0)
            {
                Vector2 offsetFromCenter = (Vector2)Input.mousePosition - new Vector2(Screen.width * 0.5f, Screen.height * 0.5f);

                targetPosition.x -= offsetFromCenter.x * zoomChange * MOVE_WITH_SCROLL_VALUE;
                targetPosition.y -= offsetFromCenter.y * zoomChange * MOVE_WITH_SCROLL_VALUE;
            }

            if(PositionEnabled)
                cachedTransform.localPosition = Vector3.Lerp(cachedTransform.localPosition, targetPosition, interpolationSpeed * unscaledDeltaTime);


            currentZoom = Mathf.Lerp(currentZoom, targetZoom, interpolationSpeed * unscaledDeltaTime);

            thisCamera.orthographicSize = currentZoom * ZoomMultiplier;
        }

        public void ResetPosition()
        {
            targetPosition = cachedTransform.localPosition;
        }
    }
}