﻿namespace MossWolf.Utility.Audio
{

    public static class AudioUtils
    {
        private const float PITCH_PER_SEMITONE = 1.05946f;
        private static readonly int[] SEMITONE_STEPS = { 0, 2, 4, 7, 9, 12 };

        /// <summary>
        /// Adds variation to a sound by returning a random pitch shift value to play a solund effect with a pentatonic scale.
        /// </summary>
        public static float GetPentatonicPitch(int maxSteps = -1)
        {
            if (maxSteps < 0 || maxSteps > SEMITONE_STEPS.Length)
                maxSteps = SEMITONE_STEPS.Length;

            int semiTones = SEMITONE_STEPS[UnityEngine.Random.Range(0, maxSteps)];
            return UnityEngine.Mathf.Pow(PITCH_PER_SEMITONE, semiTones);
        }
    }
}