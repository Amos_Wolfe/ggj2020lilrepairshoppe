﻿using MossWolf.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MossWolf.Utility.Misc
{
    public class ChooseRandomSprite : BaseBehaviour
    {
        [SerializeField]
        private List<Sprite> spriteList;

        private SpriteRenderer spriteRend;

        protected override void AwakeEx()
        {
            base.AwakeEx();

            GetComponentEx(ref spriteRend);

            if (spriteRend != null && spriteList.Count != 0)
            {
                spriteRend.sprite = spriteList[UnityEngine.Random.Range(0, spriteList.Count - 1)];
                
            }
        }
    }
}