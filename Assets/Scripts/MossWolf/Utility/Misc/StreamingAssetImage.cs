﻿using MossWolf.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MossWolf.Utility.Misc
{
	/// <summary>
	/// Only loads the sprite for the image from StreamingAssets.
	/// </summary>
	public class StreamingAssetImage : BaseBehaviour
	{
		public string filePath;

		public void Load()
		{
			string fullFilePath = Application.streamingAssetsPath + "/" + filePath;

			WWW wwwFile = new WWW("file://" + fullFilePath);
			while (!wwwFile.isDone) { }

			if (wwwFile.error != null)
			{
				LogError("Error while loading image from streaming assets '{0}': {1}", filePath, wwwFile.error);
			}
			else
			{
				var thisImage = GetComponent<Image>();

				Texture texture = wwwFile.texture;
				Sprite newSprite = Sprite.Create(texture as Texture2D, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
				thisImage.sprite = newSprite;
			}
		}

		public IEnumerator _Load()
		{
			string fullFilePath = Application.streamingAssetsPath + "/" + filePath;

			WWW wwwFile = new WWW("file://" + fullFilePath);
			while (!wwwFile.isDone)
			{
				yield return null;
			}

			if (wwwFile.error != null)
			{
				LogError("Error while loading image from streaming assets '{0}': {1}", filePath, wwwFile.error);
			}
			else
			{
				var thisImage = GetComponent<Image>();

				Texture texture = wwwFile.texture;
				Sprite newSprite = Sprite.Create(texture as Texture2D, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
				thisImage.sprite = newSprite;
			}
		}
	}
}