﻿using MossWolf.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MossWolf.Utility.Misc
{
    /// <summary>
    /// Used for objects that just spin on the y axis. Faster than Spin script and requires the object to not move.
    /// </summary>
    public class SpinYAxis : BaseBehaviour
    {
        [SerializeField, Tooltip("The spin rate in degrees per second")]
        private float speed;

        private Transform thisTransform;
        private Vector3 currentRotationEulers;

        private void Start()
        {
            // calling transform is expensive (because Unity), so store it in a field to use instead
            thisTransform = transform;
            currentRotationEulers = thisTransform.localEulerAngles;
        }

        private void Update()
        {
            currentRotationEulers.y = (currentRotationEulers.y  +speed * Time.deltaTime) % 360;
            thisTransform.localEulerAngles = currentRotationEulers;
        }
    }
}