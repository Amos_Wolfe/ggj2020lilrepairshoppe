﻿using MossWolf.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MossWolf.Utility.Misc
{
    /// <summary>
    /// A simple script that spins an object in the update loop
    /// </summary>
    public class Spin : BaseBehaviour
    {
        [SerializeField, Tooltip("The spin rate in degrees per second")]
        private Vector3 speed;

        private Transform thisTransform;

        private void Start()
        {
            // calling transform is expensive (because Unity), so store it in a field to use instead
            thisTransform = transform;
        }

        private void Update()
        {
            thisTransform.Rotate(speed * Time.deltaTime);
        }
    }
}