﻿using MossWolf.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MossWolf.Utility.UI
{
    /// <summary>
    /// used to diplay lists of UI elements.
    /// </summary>
    public class WidgetList : BaseBehaviour
    {
        [SerializeField]
        private GameObject templateWidget;
        [SerializeField]
        private int initialPoolCount = 10;

        private bool initialised;
        private Stack<GameObject> widgetPool;
        private List<GameObject> widgetList;

        protected override void AwakeEx()
        {
            Init();
        }

        private void Init()
        {
            if (initialised) return;
            initialised = true;

            base.AwakeEx();

            templateWidget.SetActive(false);

            widgetPool = new Stack<GameObject>();
            widgetList = new List<GameObject>();

            while (widgetPool.Count < initialPoolCount)
                CreateNewElement();
        }

        public T AddWidget<T>() where T : Component
        {
            Init();

            if (widgetPool.Count == 0)
                CreateNewElement();

            var newWidget = widgetPool.Pop();
            newWidget.transform.SetAsLastSibling();
            newWidget.SetActive(true);

            widgetList.Add(newWidget);

            var newWidgetComponent = newWidget.GetComponent<T>();
            if(ReferenceEquals(newWidgetComponent,null))
                LogError("Could not find {0} component on widget", typeof(T).Name);

            return newWidgetComponent;
        }

        public bool RemoveComponent<T>(T widgetComponent) where T : Component
        {
            Init();

            if (ReferenceEquals(widgetComponent, null))
            {
                LogError("Widget component is null");
                return false;
            }
            var widgetObject = widgetComponent.gameObject;

            if(widgetList.Remove(widgetObject))
            {
                widgetPool.Push(widgetObject);
                widgetObject.SetActive(false);
                return true;
            }
            return false;
        }
        
        public void Clear()
        {
            for (int i = 0; i < widgetList.Count; i++)
            {
                widgetPool.Push(widgetList[i]);
                widgetList[i].gameObject.SetActive(false);
            }

            widgetList.Clear();
        }

        private void CreateNewElement()
        {
            var newWidget = Instantiate(templateWidget, templateWidget.transform.parent, false);
            newWidget.name = string.Format("{0}_{1}", templateWidget.name, widgetPool.Count);
            widgetPool.Push(newWidget);
        }
    }
}