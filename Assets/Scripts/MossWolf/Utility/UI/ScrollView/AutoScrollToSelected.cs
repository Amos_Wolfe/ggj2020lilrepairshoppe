﻿using MossWolf.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace MossWolf.Utility.UI
{
	/// <summary>
	/// Used in combination with the AutoScroll element to always scroll to the selected object if it is within the scroll view.
	/// </summary>
	public class AutoScrollToSelected : BaseBehaviour
	{
		[SerializeField, Tooltip("If any of these keys are pressed, this component will try to auto scroll the scroll view to the selected object")]
		private KeyCode[] checkScrollKeys = new KeyCode[4] { KeyCode.LeftArrow, KeyCode.RightArrow, KeyCode.UpArrow, KeyCode.DownArrow };

		private EventSystem eventSystem;

		private AutoScroll autoScroll;

		protected override void AwakeEx()
		{
			base.AwakeEx();

			ReferenceManager.GetBehaviours(ref eventSystem);

			GetComponentEx(ref autoScroll);
		}

		private void Update()
		{
			for (int i = 0; i < checkScrollKeys.Length; i++)
			{
				if (Input.GetKey(checkScrollKeys[i]))
					CheckScroll();
			}
		}

		private void CheckScroll()
		{
			if (eventSystem == null) return;
			if (eventSystem.currentSelectedGameObject == null) return;

			var selectedObject = eventSystem.currentSelectedGameObject;

			autoScroll.AutoScrollToElement(selectedObject);
		}
	}
}