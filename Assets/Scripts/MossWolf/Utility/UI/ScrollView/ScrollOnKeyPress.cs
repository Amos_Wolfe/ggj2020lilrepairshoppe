﻿using MossWolf.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MossWolf.Utility.UI
{
	/// <summary>
	/// Makes the attached scroll view scroll vertically when keyboard keys are used
	/// </summary>
	public class ScrollOnKeyPress : BaseBehaviour
	{
		[SerializeField]
		private KeyCode[] upKeys = new KeyCode[1] { KeyCode.UpArrow };
		[SerializeField]
		private KeyCode[] downKeys = new KeyCode[1] { KeyCode.DownArrow };
		[SerializeField]
		private float scrollAmount = 1;

		private ScrollRect scrollView;

		/// <summary>
		/// An optional component. This component will be cancelled whenever any controls are being used.
		/// </summary>
		private AutoScroll autoScroll;

		protected override void AwakeEx()
		{
			base.AwakeEx();

			GetComponentEx(ref scrollView);

			autoScroll = GetComponent<AutoScroll>();
		}

		private void Update()
		{
			for (int i = 0; i < upKeys.Length; i++)
			{
				if (Input.GetKey(upKeys[i]))
					Scroll(1);
			}

			for (int i = 0; i < downKeys.Length; i++)
			{
				if (Input.GetKey(downKeys[i]))
					Scroll(-1);
			}
		}

		private void Scroll(int direction)
		{
			if (scrollView == null) return;

			if (autoScroll != null)
				autoScroll.CancelAutoScroll();

			scrollView.verticalNormalizedPosition = Mathf.Clamp01(scrollView.verticalNormalizedPosition + direction * scrollAmount * Time.unscaledDeltaTime);
		}
	}
}