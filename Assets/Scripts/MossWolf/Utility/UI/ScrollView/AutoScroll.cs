﻿using MossWolf.Base;
using MossWolf.Utility.GameObjectUtil;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MossWolf.Utility.UI
{
	/// <summary>
	/// Starts a coroutine to autoscroll to gameobjects within this scroll view.  AutoScrollToElement must be called on a gameobject within this object, otherwise it will do nothing.
	/// </summary>
	public class AutoScroll : BaseBehaviour
	{
		[SerializeField]
		private float autoScrollTime = 0.1f;

		private ScrollRect scrollView;
		private RectTransform thisRect;
		private bool autoScrolling;

		private Coroutine scrollCoroutine;

		protected override void AwakeEx()
		{
			base.AwakeEx();

			GetComponentEx(ref scrollView, ref thisRect);
		}

		/// <summary>
		/// If the target element is within this scroll view, then the scroll view will automatically scroll so that element is in the center of the view (if possible).
		/// </summary>
		public void AutoScrollToElement(GameObject targetElement)
		{
			CallAwakeEx();
			if (targetElement == null) return;

			// only continue if the object is selected within this scroll view
			if (!GameObjectUtils.ObjectExistsInParent(targetElement.transform, transform)) return;

			float targetPosition = UIUtils.FindTargetScrollPosition(targetElement, scrollView);

			CancelAutoScroll();

			scrollCoroutine = StartCoroutine(_ScrollToPosition(targetPosition));
		}

		/// <summary>
		/// Cancels the coroutine used to auto scroll to an element. Use this if player controls are being used.
		/// </summary>
		public void CancelAutoScroll()
		{
			if (autoScrolling && scrollCoroutine != null)
			{
				autoScrolling = false;
				StopCoroutine(scrollCoroutine);
			}
		}

		private IEnumerator _ScrollToPosition(float targetPosition)
		{
			autoScrolling = true;

			float startPosition = scrollView.verticalNormalizedPosition;

			float t = 0;
			while (t < 1)
			{
				float unscaledDelta = Mathf.Min(Time.unscaledDeltaTime, Time.maximumDeltaTime);
				t = Mathf.Clamp01(t + unscaledDelta / autoScrollTime);

				scrollView.verticalNormalizedPosition = Mathf.Lerp(startPosition, targetPosition, t);

				yield return null;
			}

			autoScrolling = false;
		}
	}
}
