﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace MossWolf.Utility.UI
{
	/// <summary>
	/// Convenience methods used for navigating and selecting things in the Unity UI system.
	/// </summary>
	public static class UIUtils
	{

		/// <summary>
		/// Checks all selectables within the specified object. If none of them are selected the first or last button will be selected.
		/// </summary>
		/// <param name="preferLastSelection">If set to true, the last button will be selected, not the first</param>
		public static void HighlightChoiceIfUnhighlighted(GameObject baseObject, EventSystem eventSystem, bool preferLastSelection = false)
		{
			if (baseObject == null) return;

			var selectableList = baseObject.GetComponentsInChildren<Selectable>();
			if (selectableList.Length == 0) return;

			bool buttonSelected = false;
			for (int i = 0; i < selectableList.Length; i++)
			{
				if (eventSystem.currentSelectedGameObject == selectableList[i].gameObject)
				{
					buttonSelected = true;
					break;
				}
			}

			// Select the first button if nothing is selected
			if (!buttonSelected)
			{
				if (!preferLastSelection)
					eventSystem.SetSelectedGameObject(selectableList[0].gameObject);
				else
					eventSystem.SetSelectedGameObject(selectableList[selectableList.Length - 1].gameObject);
			}
		}

		/// <summary>
		/// Returns a number between 0 and 1 (where 0 is the bottom of the scroll view, and 1 is the top), showing where to scroll to to see the target element.
		/// </summary>
		/// <param name="targetElement">The target element to scroll to.</param>
		/// <param name="scrollView">The scroll view that contents the target element</param>
		/// <returns></returns>
		public static float FindTargetScrollPosition(GameObject targetElement, ScrollRect scrollView)
		{
			if (scrollView == null)
			{
				Debug.LogError("[FindTargetScrollPosition] No ScrollView found");
				return 0;
			}
			if (scrollView.content == null)
			{
				Debug.LogError("[FindTargetScrollPosition] No ScrollView content found");
				return 0;
			}
			if (scrollView.viewport == null)
			{
				Debug.LogError("[FindTargetScrollPosition] No ScrollView viewport found");
				return 0;
			}

			float contentScale = scrollView.content.localScale.y;

			float viewportHeight = scrollView.viewport.rect.height;
			float contentHeight = scrollView.content.rect.height * contentScale;

			RectTransform selectedRect = targetElement.GetComponent<RectTransform>();
			if (selectedRect == null) return 0;

			var relativeBounds = RectTransformUtility.CalculateRelativeRectTransformBounds(scrollView.content, selectedRect);
			float relativePosition = relativeBounds.center.y * contentScale;

			float contentPivotY = scrollView.content.pivot.y;
			relativePosition += contentHeight * contentPivotY;

			relativePosition -= viewportHeight / 2;

			float scrollableHeight = contentHeight - viewportHeight;
			float relativeNormalisedPosition = relativePosition / scrollableHeight;

			float targetPosition = Mathf.Clamp01(relativeNormalisedPosition);

			return targetPosition;
		}

        /// <summary>
        /// Returns true if the mouse is currently hovering over a UI element.
        /// </summary>
        public static bool IsMouseOverUI()
        {
            return EventSystem.current.IsPointerOverGameObject();
        }
	}
}