﻿using MossWolf.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace MossWolf.Utility.UI
{
	/// <summary>
	/// Automatically highlights this object when it becomes activated. Used for things such as keyboard and gamepad controls, where UI navigation is used.
	/// </summary>
	public class AutoHighlightOnEnable : BaseBehaviour
	{
		private EventSystem eventSystem;

		private bool selectNextFrame;

		protected override void AwakeEx()
		{
			base.AwakeEx();
			ReferenceManager.GetBehaviours(ref eventSystem);
		}

		private void OnEnable()
		{
			CallAwakeEx();

			selectNextFrame = true;
		}

		private void Update()
		{
			if (selectNextFrame)
			{
				selectNextFrame = false;

				if (eventSystem != null)
					eventSystem.SetSelectedGameObject(gameObject);
			}
		}
	}
}