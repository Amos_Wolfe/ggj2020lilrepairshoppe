﻿using MossWolf.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace MossWolf.Utility.UI
{
	/// <summary>
	/// The Unity event system keeps the last button clicked on selected/highlighted. This can be confusing for keyboard and mouse controls. This component frequently deselects the currently selected object from the event system.
	/// 
	/// This is only used if the mouse moves, so that the keyboard can still be used to navigate stuff.
	/// </summary>
	public class KeepObjectsUnselected : BaseBehaviour
	{
		private EventSystem eventSystem;

		private Vector3 lastMousePos;

		protected override void AwakeEx()
		{
			base.AwakeEx();

			GetComponentEx(ref eventSystem);
		}

		private void Update()
		{
			if (Input.mousePosition == lastMousePos) return;
			lastMousePos = Input.mousePosition;

			if (eventSystem != null && eventSystem.currentSelectedGameObject != null)
				eventSystem.SetSelectedGameObject(null);
		}
	}
}