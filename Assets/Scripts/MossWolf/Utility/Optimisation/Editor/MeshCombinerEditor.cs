﻿using MossWolf.Base;
using MossWolf.Utility.Optimisation;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CanEditMultipleObjects]
[CustomEditor(typeof(MeshCombiner), true)]
public class MeshCombinerEditor : BaseBehaviourEditor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("Combine Meshes"))
        {
            var meshCombiner = target as MeshCombiner;
            meshCombiner.StartCombine();
        }
    }
}
