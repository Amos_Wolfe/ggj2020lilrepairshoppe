﻿using MossWolf.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace MossWolf.Utility.Optimisation
{
    /// <summary>
    /// Combines all mesh renderers in this object's child objects (not in the object this script is attached to though) into one mesh. This will disable the original mesh filter objects after combining them.
    /// </summary>
    public class MeshCombiner : BaseBehaviour
    {
        private Mesh combinedMesh;
        private Material[] materials;
        private string filePath;

        public override string HelpText => "Combines all mesh filters in this object's child objects (not in the object this script is attached to though) into one mesh. This will disable the original mesh filter objects after combining them.";

        public void StartCombine()
        {
#if UNITY_EDITOR
            if (!ChooseSaveFilePath()) return;
            CollectMeshes();
            SaveCombinedMesh();
            UseCombinedMesh();
#endif
        }

        private bool ChooseSaveFilePath()
        {
#if UNITY_EDITOR
            filePath =
        EditorUtility.SaveFilePanelInProject("Save Procedural Mesh", "CombinedMesh", "asset", "Choose a location for the new combined mesh");

            return !string.IsNullOrEmpty(filePath);
#else
            return false;
#endif
        }

        private void CollectMeshes()
        {
#if UNITY_EDITOR
            var vertexList = new List<Vector3>();
            var triangleList = new List<int>();
            var normalList = new List<Vector3>();
            var uv1List = new List<Vector2>();

            var newVertexList = new List<Vector3>();
            var newTriangleList = new List<int>();

            foreach (Transform child in this.transform)
            {
                var localScale = child.localScale;
                var localRotation = child.localRotation;
                var localPosition = child.localPosition;

                var meshFilterList = child.GetComponents<MeshFilter>();
                foreach(var meshFilter in meshFilterList)
                {
                    var newMesh = meshFilter.sharedMesh;
                    if (newMesh != null)
                    {
                        newVertexList.Clear();
                        newTriangleList.Clear();

                        var newVertexArray = newMesh.vertices;

                        for (int i = 0; i < newVertexArray.Length; i++)
                        {
                            var newVertex = newVertexArray[i];
                            newVertex.x *= localScale.x;
                            newVertex.y *= localScale.y;
                            newVertex.z *= localScale.z;

                            newVertex = localRotation * newVertex;

                            newVertex += localPosition;

                            newVertexList.Add(newVertex);
                        }

                        int offset = vertexList.Count;
                        var newTriangleArray = newMesh.triangles;
                        for(int i = 0; i < newTriangleArray.Length; i++)
                        {
                            newTriangleList.Add(newTriangleArray[i] + offset);
                        }

                        vertexList.AddRange(newVertexList);
                        triangleList.AddRange(newTriangleList);
                        normalList.AddRange(newMesh.normals);
                        uv1List.AddRange(newMesh.uv);
                    }
                }

                var meshRenderer = child.GetComponent<MeshRenderer>();
                if (meshRenderer != null)
                {
                    if (materials == null)
                    {

                        materials = meshRenderer.sharedMaterials;
                    }

                    DestroyImmediate(meshRenderer);
                }

                for (int i = 0; i < meshFilterList.Length; i++)
                    DestroyImmediate(meshFilterList[i]);
            }

            combinedMesh = new Mesh();
            combinedMesh.name = "CombinedMesh";
            combinedMesh.SetVertices(vertexList);
            combinedMesh.SetTriangles(triangleList, 0);
            combinedMesh.SetNormals(normalList);
            combinedMesh.SetUVs(0, uv1List);

            Unwrapping.GenerateSecondaryUVSet(combinedMesh);

            combinedMesh.RecalculateBounds();
#endif
        }

        private void SaveCombinedMesh()
        {
#if UNITY_EDITOR
            AssetDatabase.CreateAsset(combinedMesh, filePath);
#endif
        }

        private void UseCombinedMesh()
        {
#if UNITY_EDITOR
            var newMeshFilter = gameObject.AddComponent<MeshFilter>();
            newMeshFilter.sharedMesh = combinedMesh;

            var newMeshRenderer = gameObject.AddComponent<MeshRenderer>();
            newMeshRenderer.sharedMaterials = materials;
#endif
        }
    }
}