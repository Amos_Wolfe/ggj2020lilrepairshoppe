﻿using MossWolf.Base;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace MossWolf.Utility.Tilemaps
{
    /// <summary>
    /// Handles opening and closing DoorTiles
    /// </summary>
    public class DoorManager : BaseBehaviour
    {
        public class DoorTileInfo
        {
            public Tilemap Tilemap;
            public DoorTile DoorTile;
            public Vector3Int Position;
            public float CloseTimeLeft;
            public int CurrentAnimationIndex;
            public int MaxAnimationIndex;
        }

        [SerializeField, Tooltip("The amount of seconds to wait to close the door after opening it")]
        private float closeDelaySeconds = 1;
        [SerializeField, Tooltip("The delay in seconds between each door open and close animation frame")]
        private float delaySecondsPerFrame = 0.05f;

        private List<DoorTileInfo> doorTiles;
        private float nextUpdate;

        protected override void AwakeEx()
        {
            base.AwakeEx();

            doorTiles = new List<DoorTileInfo>();
        }

        private void Update()
        {
            if (Time.time < nextUpdate) return;
            nextUpdate = Time.time + delaySecondsPerFrame;

            for(int i = 0; i < doorTiles.Count; i++)
            {
                var currentDoor = doorTiles[i];
                var doorTile = currentDoor.DoorTile;
                if(currentDoor.CloseTimeLeft > 0)
                {
                    // animate door opening
                    currentDoor.CloseTimeLeft -= delaySecondsPerFrame;

                    currentDoor.CurrentAnimationIndex = Mathf.Clamp(currentDoor.CurrentAnimationIndex + 1, 0, currentDoor.MaxAnimationIndex);

                    doorTile.AnimateDoor(true, currentDoor.Tilemap, currentDoor.Position, currentDoor.CurrentAnimationIndex);
                }
                else
                {
                    // animate door closing
                    currentDoor.CurrentAnimationIndex = Mathf.Clamp(currentDoor.CurrentAnimationIndex - 1, 0, currentDoor.MaxAnimationIndex);

                    doorTile.AnimateDoor(false, currentDoor.Tilemap, currentDoor.Position, currentDoor.CurrentAnimationIndex);
                    // If fully closed, remove this door from the list
                    if(currentDoor.CurrentAnimationIndex == 0)
                    {
                        doorTiles.RemoveAt(i);
                        --i;
                    }
                }
            }
        }

        public void OpenDoor(DoorTile newDoor, Tilemap tilemap, Vector3Int position)
        {
            int tilemapID = tilemap.GetInstanceID();

            var currentDoor = doorTiles.Find(x => x.Tilemap.GetInstanceID() == tilemapID && x.Position == position);
            if (ReferenceEquals(currentDoor,null))
            {
                currentDoor = new DoorTileInfo { Tilemap = tilemap, DoorTile = newDoor, Position = position, MaxAnimationIndex = newDoor.DoorSprites.Length -1 };
                doorTiles.Add(currentDoor);
            }

            currentDoor.CloseTimeLeft = closeDelaySeconds;
        }
    }
}