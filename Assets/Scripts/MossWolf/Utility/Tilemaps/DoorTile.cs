﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace MossWolf.Utility.Tilemaps
{
    [System.Serializable]
    [CreateAssetMenu(fileName = "New Door Tile", menuName = "Tiles/Door Tile")]
    public class DoorTile : TileBase
    {
        [Tooltip("Door sprite animation images, from closed to open. The door needs to connect from left wall to a right wall (it will be auto rotated in game to match where the walls are")]
        public Sprite[] DoorSprites;

        private int currentAnimationIndex;
        private bool onlySetAnimation;

        public void AnimateDoor(bool open, Tilemap tilemap, Vector3Int position, int animationIndex)
        {
            this.currentAnimationIndex = animationIndex;
            onlySetAnimation = true;
            tilemap.RefreshTile(position);
            onlySetAnimation = false;
        }

        public override void GetTileData(Vector3Int position, ITilemap tilemap, ref TileData tileData)
        {
            if (DoorSprites == null || DoorSprites.Length == 0) return;

#if UNITY_EDITOR
            if (!Application.isPlaying)
                currentAnimationIndex = 0;
#endif

            tileData.sprite = DoorSprites[currentAnimationIndex];

            if (onlySetAnimation) return;

            tileData.flags = TileFlags.LockTransform;

            float angle = 0;

            bool left = IsWallTile(tilemap, position + new Vector3Int(-1, 0, 0));
            bool right = IsWallTile(tilemap, position + new Vector3Int(1, 0, 0));
            bool up = IsWallTile(tilemap, position + new Vector3Int(0, 1, 0));
            bool down = IsWallTile(tilemap, position + new Vector3Int(0, -1, 0));

            int leftRightScore = 0;
            if (left)
                leftRightScore++;
            if (right)
                leftRightScore++;

            int upDownScore = 0;
            if (up)
                upDownScore++;
            if (down)
                upDownScore++;

            if (upDownScore > leftRightScore)
                angle = up ? -90 : 90;
            else
                angle = left ? 0 : 180;

            tileData.transform = Matrix4x4.Rotate(Quaternion.Euler(0, 0, angle));
        }

        private bool IsWallTile(ITilemap tilemap, Vector3Int position)
        {
            var tile = tilemap.GetTile(position);
            if (ReferenceEquals(tile, null)) return false;
            if (tile is DoorTile) return false;

            return true;
        }
    }
}