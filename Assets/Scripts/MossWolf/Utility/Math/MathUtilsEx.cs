﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MossWolf.Utility.Math
{
	public static class MathUtilsEx
	{
		/// <summary>
		/// Snaps each axis to the specified grid point size.
		/// </summary>
		public static void SnapToGridRound(ref Vector3 position, float gridSize)
		{
			SnapToGridRound(ref position.x, gridSize);
			SnapToGridRound(ref position.y, gridSize);
			SnapToGridRound(ref position.z, gridSize);
		}

		/// <summary>
		/// Snaps each axis to the specified grid point size.
		/// </summary>
		public static void SnapToGridRound(ref Vector2 position, float gridSize)
		{
			SnapToGridRound(ref position.x, gridSize);
			SnapToGridRound(ref position.y, gridSize);
		}

        /// <summary>
        /// Snaps the float to the specified grid point size.
        /// </summary>
        public static void SnapToGridRound(ref float floatValue, float gridSize)
        {
            floatValue = Mathf.Round(floatValue / gridSize) * gridSize;
        }

        /// <summary>
		/// Snaps each axis to the specified grid point size.
		/// </summary>
		public static void SnapToGridFloor(ref Vector3 position, float gridSize)
        {
            SnapToGridFloor(ref position.x, gridSize);
            SnapToGridFloor(ref position.y, gridSize);
            SnapToGridFloor(ref position.z, gridSize);
        }

        /// <summary>
        /// Snaps each axis to the specified grid point size.
        /// </summary>
        public static void SnapToGridFloor(ref Vector2 position, float gridSize)
        {
            SnapToGridFloor(ref position.x, gridSize);
            SnapToGridFloor(ref position.y, gridSize);
        }

        /// <summary>
		/// Snaps the float to the specified grid point size.
		/// </summary>
		public static void SnapToGridFloor(ref float floatValue, float gridSize)
        {
            floatValue = Mathf.Floor(floatValue / gridSize) * gridSize;
        }

        /// <summary>
		/// Snaps each axis to the specified grid point size.
		/// </summary>
		public static void SnapToGridCeil(ref Vector3 position, float gridSize)
        {
            SnapToGridCeil(ref position.x, gridSize);
            SnapToGridCeil(ref position.y, gridSize);
            SnapToGridCeil(ref position.z, gridSize);
        }

        /// <summary>
        /// Snaps each axis to the specified grid point size.
        /// </summary>
        public static void SnapToGridCeil(ref Vector2 position, float gridSize)
        {
            SnapToGridCeil(ref position.x, gridSize);
            SnapToGridCeil(ref position.y, gridSize);
        }

        /// <summary>
		/// Snaps the float to the specified grid point size.
		/// </summary>
		public static void SnapToGridCeil(ref float floatValue, float gridSize)
        {
            floatValue = Mathf.Ceil(floatValue / gridSize) * gridSize;
        }
    }
}