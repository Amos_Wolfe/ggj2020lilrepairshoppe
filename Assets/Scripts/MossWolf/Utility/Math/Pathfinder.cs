﻿using MossWolf.Base;
using System.Collections.Generic;
using UnityEngine;

namespace MossWolf.Utility.Math
{
    public enum PathfinderStatus
    {
        None = 0,
        InProgress = 1,
        Success = 2,
        Failure = 3,
    }

    public enum PathfindingMode
    {
        None = -1,
        /// <summary>
        /// Finds a path around the obstacle tiles using A Star pathfinding
        /// </summary>
        AStarPath = 0,
        /// <summary>
        /// Searches multiple paths until a spot that doesn't have a floor tile is found
        /// </summary>
        FindNearestExit,
        /// <summary>
        /// Searches multiple paths until a spot that has a floor tile is found
        /// </summary>
        FindNearestEntrance,
    }

    /// <summary>
    /// Information about the structure the pathfinder has to navigate around
    /// </summary>
    public struct PathfinderStructure
    {
        public bool[] WalkableTiles;
        public bool[] FloorTiles;
        public int XLeft;
        public int XRight;
        public int YBottom;
        public int YTop;
        public int BoundsWidth;
        public int BoundsHeight;

        public bool IsTileWalkable(Vector3Int position, bool checkFloor)
        {
            int x = position.x;
            int y = position.y;

            if (x < XLeft) return !checkFloor;
            if (x >= XRight) return !checkFloor;
            if (y < YBottom) return !checkFloor;
            if (y >= YTop) return !checkFloor;

            int tileIndex = (x - XLeft) + (y - YBottom) * (BoundsWidth + 1);

            if (checkFloor)
            {
                if (!FloorTiles[tileIndex]) return false;
            }

            return WalkableTiles[tileIndex];
        }

        public bool IsFloorAtPosition(Vector3Int position)
        {
            int x = position.x;
            int y = position.y;

            if (x < XLeft) return false;
            if (x >= XRight) return false;
            if (y < YBottom) return false;
            if (y >= YTop) return false;

            int tileIndex = (x - XLeft) + (y - YBottom) * (BoundsWidth + 1);
            return FloorTiles[tileIndex];
        }
    }

    /// <summary>
    /// Similar to PathfindingUtils, except this component will pathfind over multiple frames, reducing lag spikes.
    /// </summary>
    public class Pathfinder : BaseBehaviour
    {
        private const int ESTIMATED_TILES = 10000;
        private const int ESTIMATED_PATH_SIZE = 100;

        [SerializeField, Tooltip("Increasing this decreases the amount of time taken to pathfind, but will decrease performance as well.")]
        private int tileChecksPerFrame = 10;

        [SerializeField]
        private bool speedUpWithTimeScale = true;
        [SerializeField, Tooltip("Creates a particle system if set to true, and spawns particles at the path points it checks")]
        private DebugParticleInfo DebugInfo;

        private List<Vector3> pathPointList;

        private Vector3Int startTilePosition;
        private Vector3Int targetTilePosition;
        private int maxSearchIterations;
        private int iterations;
        private int targetDistance;
        private List<PathfindingNode> openList;
        private List<PathfindingNode> closedList;

        private PathfinderStructure pathfinderStructure;

        private bool walkOnFloor;

        private DebugParticles debugParticles;

        private bool inProgress;

        public PathfinderStatus Status { get; private set; }

        /// <summary>
        /// Determines what this pathfinder is currently being used for.
        /// </summary>
        public PathfindingMode Mode { get; private set; }

        public Vector3Int TargetTilePosition => targetTilePosition;

        protected override void AwakeEx()
        {
            base.AwakeEx();

            pathPointList = new List<Vector3>(ESTIMATED_PATH_SIZE);
            openList = new List<PathfindingNode>(ESTIMATED_TILES);
            closedList = new List<PathfindingNode>(ESTIMATED_TILES);

            debugParticles = new DebugParticles(DebugInfo, transform);
        }

        private void Update()
        {
            if (inProgress)
            {
                int currentIterations = 0;
                int checksThisFrame = (int)(Time.timeScale * tileChecksPerFrame);
                while (inProgress && currentIterations < checksThisFrame)
                {
                    currentIterations++;
                    NextIteration();
                }
            }
        }

        public void GetPathPoints(List<Vector3> newPathPointList)
        {
            if(Status != PathfinderStatus.Success)
            {
                LogError("Path hasn't been generated successfully yet");
                return;
            }
            newPathPointList.Clear();
            newPathPointList.AddRange(pathPointList);
        }

        public void ResetStatus()
        {
            Status = PathfinderStatus.None;
        }

        /// <summary>
        /// Uses a star to find a path froom the start to the target position
        /// </summary>
        /// <param name="startLocalPosition">The starting position local to the tilemap</param>
        /// <param name="targetLocalPosition">The target position local to the tilemap</param>
        /// <param name="obstacleTilemap">The tilemap that must be navigated around. All tiles that are not DoorTiles will not be walkable</param>
        /// <param name="floorTileMap">If set to a value other than null, this tilemap will be used to determine walkable tiles.</param>
        /// <param name="maxSearchIterations">The amount of iterations to give up at.</param>
        /// <param name="targetDistance">Set this to 0 if a path to the exact target position tile is needed. If only a path near the target location is required, set this to the number of tiles for the path to end from the target location.</param>
        /// <returns></returns>
        public bool FindPath(Vector3 startLocalPosition, Vector3 targetLocalPosition, PathfinderStructure pathfinderStructure, int maxSearchIterations, int targetDistance, bool walkOnFloor)
        {
            Mode = PathfindingMode.AStarPath;

            var newStartTilePosition = LocalToCell(startLocalPosition);
            var newTargetTilePosition = LocalToCell(targetLocalPosition);

            this.pathfinderStructure = pathfinderStructure;

            bool tileAtEndBlocked = !pathfinderStructure.IsTileWalkable(newTargetTilePosition, false);
            if (tileAtEndBlocked && targetDistance == 0) return false;

            inProgress = true;
            this.maxSearchIterations = maxSearchIterations;
            this.targetDistance = targetDistance;
            this.walkOnFloor = walkOnFloor;

            this.iterations = 0;

            Status = PathfinderStatus.InProgress;
            pathPointList.Clear();
            this.openList.Clear();
            this.closedList.Clear();

            bool tileAtStartBlocked = !pathfinderStructure.IsTileWalkable(newStartTilePosition, walkOnFloor);
            if (tileAtStartBlocked)
                newStartTilePosition = FindAvailableAdjacentStartPosition(newStartTilePosition);

            startTilePosition = newStartTilePosition;
            targetTilePosition = newTargetTilePosition;

            // Add the starting node to the open list
            var startingPoint = new PathfindingNode { G = 0, Position = startTilePosition };
            startingPoint.CalculateH(targetTilePosition);
            openList.Add(startingPoint);

            debugParticles.UpdateTransformParent();

            return true;
        }

        /// <summary>
        /// Keeps searching outwards until a tile without a floor is found.
        /// </summary>
        /// <param name="startLocalPosition">The starting position local to the tilemap</param>
        /// <param name="obstacleTilemap">The tilemap that must be navigated around. All tiles that are not DoorTiles will not be walkable</param>
        /// <param name="floorTilemap">This tilemap will be searched in order to find an exit. Tiles without a floor tile will be considered an exit</param>
        /// <param name="maxSearchIterations">The amount of iterations to give up at.</param>
        public void FindNearestExit(Vector3 startLocalPosition, PathfinderStructure pathfinderStructure, int maxSearchIterations)
        {
            DijkstraPath(PathfindingMode.FindNearestExit, startLocalPosition, pathfinderStructure, maxSearchIterations);
        }

        /// <summary>
        /// Keeps searching outwards until a tile with a floor is found.
        /// </summary>
        /// <param name="startLocalPosition">The starting position local to the tilemap</param>
        /// <param name="obstacleTilemap">The tilemap that must be navigated around. All tiles that are not DoorTiles will not be walkable</param>
        /// <param name="floorTilemap">This tilemap will be searched in order to find an entrance. Tiles with a floor tile will be considered an entrance</param>
        /// <param name="maxSearchIterations">The amount of iterations to give up at.</param>
        public void FindNearestEntrance(Vector3 startLocalPosition, PathfinderStructure pathfinderStructure, int maxSearchIterations)
        {
            DijkstraPath(PathfindingMode.FindNearestEntrance, startLocalPosition, pathfinderStructure, maxSearchIterations);
        }

        private void DijkstraPath(PathfindingMode mode, Vector3 startLocalPosition, PathfinderStructure pathfinderStructure, int maxSearchIterations)
        {
            this.Mode = mode;

            var newStartTilePosition = LocalToCell(startLocalPosition);

            inProgress = true;
            this.pathfinderStructure = pathfinderStructure;
            this.maxSearchIterations = maxSearchIterations;
            this.walkOnFloor = false;

            this.iterations = 0;

            Status = PathfinderStatus.InProgress;
            pathPointList.Clear();
            this.openList.Clear();
            this.closedList.Clear();

            bool tileAtStartBlocked = !pathfinderStructure.IsTileWalkable(newStartTilePosition, walkOnFloor);
            if (tileAtStartBlocked)
                newStartTilePosition = FindAvailableAdjacentStartPosition(newStartTilePosition);

            startTilePosition = newStartTilePosition;

            // Add the starting node to the open list
            var startingPoint = new PathfindingNode { G = 0, Position = startTilePosition };
            startingPoint.CalculateH(targetTilePosition);
            openList.Add(startingPoint);

            debugParticles.UpdateTransformParent();
        }

        private void NextIteration()
        {
            iterations++;
            if(iterations > maxSearchIterations)
            {
                inProgress = false;
                Mode = PathfindingMode.None;
                Status = PathfinderStatus.Failure;
                return;
            }

            if(openList.Count == 0)
            {
                inProgress = false;
                Mode = PathfindingMode.None;
                Status = PathfinderStatus.Failure;
                return;
            }

            switch(Mode)
            {
                case PathfindingMode.AStarPath:
                    NextIterationAStarPath();
                    break;
                case PathfindingMode.FindNearestExit:
                    NextIterationFindNearestFloorTile();
                    break;
                case PathfindingMode.FindNearestEntrance:
                    NextIterationFindNearestFloorTile();
                    break;
            }

            
        }

        private void NextIterationAStarPath()
        {
            // Get the lowest cost node
            PathfindingNode currentNode = GetLowestCostNode(openList);

            if (DebugInfo.DebugMode)
            {
                var localPosition = currentNode.Position + new Vector3(0.5f,0.5f,0);
                debugParticles.EmitAtLocation(localPosition);
            }

            // Switch it to the closed list
            int currentOpenIndex = openList.FindIndex(x => x.Position == currentNode.Position);
            if (currentOpenIndex != -1)
                openList.RemoveAt(currentOpenIndex);
            closedList.Add(currentNode);

            int distance = Mathf.Max(
                Mathf.Abs(currentNode.Position.x - targetTilePosition.x),
                Mathf.Abs(currentNode.Position.y - targetTilePosition.y));
            if (distance <= targetDistance + 0.001f)
            {
                // The path has been found, get the list of nodes
                GetNodeList(currentNode);
                inProgress = false;
                Mode = PathfindingMode.None;
                Status = PathfinderStatus.Success;
                return;
            }

            AddAdjacentToOpenList(currentNode, openList, closedList, targetTilePosition);
        }

        private void NextIterationFindNearestFloorTile()
        {
            // Get the lowest cost node
            PathfindingNode currentNode = GetLowestCostNode(openList);

            if (DebugInfo.DebugMode)
            {
                var localPosition = currentNode.Position + new Vector3(0.5f, 0.5f, 0);
                debugParticles.EmitAtLocation(localPosition);
            }

            // Switch it to the closed list
            int currentOpenIndex = openList.FindIndex(x => x.Position == currentNode.Position);
            if (currentOpenIndex != -1)
                openList.RemoveAt(currentOpenIndex);
            closedList.Add(currentNode);

            // If finding an entrance, then look for a floor tile. Otherwise if looking for an exit, look for a spot without a floor tile.
            bool targetFound = pathfinderStructure.IsFloorAtPosition(currentNode.Position);
            if (Mode == PathfindingMode.FindNearestExit)
                targetFound = !targetFound;

            if (targetFound)
            {
                // The path has been found, get the list of node
                GetNodeList(currentNode);
                inProgress = false;
                Mode = PathfindingMode.None;
                Status = PathfinderStatus.Success;
                return;
            }

            AddAdjacentToOpenList(currentNode, openList, closedList, targetTilePosition);
        }

        /// <summary>
        /// Called when the target tile has been found. Travels backwards through the list to get the path.
        /// </summary>
        private void GetNodeList(PathfindingNode currentNode)
        {
            var endPathNode = currentNode;
            pathPointList.Add(endPathNode.Position + new Vector3(0.5f, 0.5f, 0));
            while (endPathNode.Parent != null)
            {
                endPathNode = endPathNode.Parent;
                pathPointList.Add(endPathNode.Position + new Vector3(0.5f, 0.5f, 0));
            }

            pathPointList.Reverse();        }

        private PathfindingNode GetLowestCostNode(List<PathfindingNode> openList)
        {
            PathfindingNode lowestCostNode = null;
            float lowestCost = float.MaxValue;

            for (int i = 0; i < openList.Count; i++)
            {
                var currentNode = openList[i];

                if (currentNode.F < lowestCost)
                {
                    lowestCost = currentNode.F;
                    lowestCostNode = currentNode;
                }
            }
            return lowestCostNode;
        }

        private void AddAdjacentToOpenList(PathfindingNode currentNode, List<PathfindingNode> openList, List<PathfindingNode> closedList, Vector3Int targetTilePosition)
        {
            bool down = AddAdjacentToOpenList(currentNode, openList, closedList, targetTilePosition, 0, -1, 1);
            bool up = AddAdjacentToOpenList(currentNode, openList, closedList, targetTilePosition, 0, 1, 1);
            bool left = AddAdjacentToOpenList(currentNode, openList, closedList, targetTilePosition ,-1, 0, 1);
            bool right = AddAdjacentToOpenList(currentNode, openList, closedList, targetTilePosition, 1, 0, 1);

            if (left && down)
                AddAdjacentToOpenList(currentNode, openList, closedList, targetTilePosition, -1, -1, 1.5f);
            if (right && down)
                AddAdjacentToOpenList(currentNode, openList, closedList, targetTilePosition, 1, -1, 1.5f);
            if (left && up)
                AddAdjacentToOpenList(currentNode, openList, closedList, targetTilePosition, -1, 1, 1.5f);
            if (right && up)
                AddAdjacentToOpenList(currentNode, openList, closedList, targetTilePosition, 1, 1, 1.5f);
        }

        private bool AddAdjacentToOpenList(PathfindingNode currentNode, List<PathfindingNode> openList, List<PathfindingNode> closedList, Vector3Int targetTilePosition, int offsetX, int offsetY, float moveCost)
        {
            var newPosition = currentNode.Position + new Vector3Int(offsetX, offsetY, 0);

            // don't go out of bounds
            if (newPosition.x < pathfinderStructure.XLeft - 1) return false;
            if (newPosition.x > pathfinderStructure.XRight + 1) return false;
            if (newPosition.y < pathfinderStructure.YBottom - 1) return false;
            if (newPosition.y > pathfinderStructure.YTop + 1) return false;

            bool walkable = pathfinderStructure.IsTileWalkable(newPosition, walkOnFloor);
            if (!walkable) return false;

            bool inClosedList = closedList.FindIndex(x => x.Position == newPosition) != -1;
            if (inClosedList) return false;

            PathfindingNode newNode = new PathfindingNode { Position = newPosition, G = currentNode.G + moveCost, Parent = currentNode };
            newNode.CalculateH(targetTilePosition);

            int openListIndex = openList.FindIndex(x => x.Position == newNode.Position);
            bool inOpenList = openListIndex != -1;

            if (inOpenList)
            {
                if (newNode.G < openList[openListIndex].G)
                {
                    openList[openListIndex] = newNode;
                }
            }
            else
            {
                openList.Add(newNode);
            }

            return true;
        }

        /// <summary>
        /// Finds an available starting position if the current one is blocked.
        /// </summary>
        private Vector3Int FindAvailableAdjacentStartPosition(Vector3Int startPosition)
        {
            if (pathfinderStructure.IsTileWalkable(startPosition + new Vector3Int(-1, -1, 0), false))
                return startPosition + new Vector3Int(-1, -1, 0);
            if (pathfinderStructure.IsTileWalkable(startPosition + new Vector3Int(-1, -1, 0), false))
                return startPosition + new Vector3Int(0, -1, 0);
            if (pathfinderStructure.IsTileWalkable(startPosition + new Vector3Int(-1, -1, 0), false))
                return startPosition + new Vector3Int(1, -1, 0);

            if (pathfinderStructure.IsTileWalkable(startPosition + new Vector3Int(-1, -1, 0), false))
                return startPosition + new Vector3Int(-1, 0, 0);

            if (pathfinderStructure.IsTileWalkable(startPosition + new Vector3Int(-1, -1, 0), false))
                return startPosition + new Vector3Int(1, 0, 0);

            if (pathfinderStructure.IsTileWalkable(startPosition + new Vector3Int(-1, -1, 0), false))
                return startPosition + new Vector3Int(-1, 1, 0);
            if (pathfinderStructure.IsTileWalkable(startPosition + new Vector3Int(-1, -1, 0), false))
                return startPosition + new Vector3Int(0, 1, 0);
            if (pathfinderStructure.IsTileWalkable(startPosition + new Vector3Int(-1, -1, 0), false))
                return startPosition + new Vector3Int(1, 1, 0);

            // resort to using the start position if an adjacent position can't be found
            return startPosition;
        }

        private Vector3Int LocalToCell(Vector3 localPosition)
        {
            return new Vector3Int(Mathf.FloorToInt(localPosition.x), Mathf.FloorToInt(localPosition.y), 0);
        }
    }
}