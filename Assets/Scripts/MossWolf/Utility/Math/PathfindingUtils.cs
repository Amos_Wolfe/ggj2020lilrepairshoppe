﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace MossWolf.Utility.Math
{
    public class PathfindingNode
    {
        /// <summary>
        /// The grid position of the node
        /// </summary>
        public Vector3Int Position;
        /// <summary>
        /// The distance to travel to get from the start node to this node
        /// </summary>
        public float G;
        /// <summary>
        /// The estimated distance to travel to get to the end node.
        /// </summary>
        public float H;
        /// <summary>
        /// The start and end distance combined.
        /// </summary>
        public float F;

        /// <summary>
        /// The node that comes before this node. This will be null for the starting node.
        /// </summary>
        public PathfindingNode Parent;

        public void CalculateH(Vector3Int EndPosition)
        {
            int distX = Mathf.Abs(Position.x - EndPosition.x);
            int distY = Mathf.Abs(Position.y - EndPosition.y);
            H = distX + distY;

            F = G + H;
        }

        public override string ToString()
        {
            return string.Format("[Pathfinding Node: Position {0}, G {1}, H {2}, F {3}]", Position, G, H, F);
        }
    }

    /// <summary>
    /// Convenience methods for pathfinding through a Unity Tilemap
    /// </summary>
    public static class PathfindingUtils
    {
        public static bool GetPath(Vector3 startLocalPosition, Vector3 targetLocalPosition, Tilemap obstacleTilemap, List<Vector3> localPathPointList, int maxSearchIterations, int targetDistance)
        {
            if (localPathPointList == null)
            {
                Debug.LogErrorFormat("[PathfindingUtils.GetPath] Local path point list not set");
                return false;
            }

            var startTilePosition = obstacleTilemap.LocalToCell(startLocalPosition);
            var targetTilePosition = obstacleTilemap.LocalToCell(targetLocalPosition);

            bool tileAtStartBlocked = obstacleTilemap.HasTile(startTilePosition);
            if (tileAtStartBlocked) return false;

            bool tileAtEndBlocked = obstacleTilemap.HasTile(targetTilePosition);
            if (tileAtEndBlocked && targetDistance == 0) return false;

            localPathPointList.Clear();

            var openList = new List<PathfindingNode>();
            var closedList = new List<PathfindingNode>();

            // Add the starting node to the open list
            var startingPoint = new PathfindingNode { G = 0, Position = startTilePosition };
            startingPoint.CalculateH(targetTilePosition);
            openList.Add(startingPoint);

            int iterations = 0;
            // Repeat until target tile is found or there are no more path points
            while(openList.Count != 0 && iterations < maxSearchIterations)
            {
                iterations++;

                // Get the lowest cost node
                PathfindingNode currentNode = GetLowestCostNode(openList);

                // Switch it to the closed list
                int currentOpenIndex = openList.FindIndex(x => x.Position == currentNode.Position);
                openList.RemoveAt(currentOpenIndex);
                closedList.Add(currentNode);

                int distance = Mathf.Max(
                    Mathf.Abs(currentNode.Position.x - targetTilePosition.x),
                    Mathf.Abs(currentNode.Position.y - targetTilePosition.y));
                if(distance <= targetDistance)
                {
                    // The path has been found, get the list of nodes
                    var endPathNode = currentNode;
                    localPathPointList.Add(obstacleTilemap.CellToLocal(endPathNode.Position) + new Vector3(0.5f,0.5f,0));
                    while(endPathNode.Parent != null)
                    {
                        endPathNode = endPathNode.Parent;
                        localPathPointList.Add(obstacleTilemap.CellToLocal(endPathNode.Position) + new Vector3(0.5f, 0.5f, 0));
                    }

                    localPathPointList.Reverse();
                    return true;
                }

                AddAdjacentToOpenList(currentNode, openList, closedList, targetTilePosition, obstacleTilemap);
            }

            // Failed to find path
            return false;
        }

        public static void RefinePath(List<Vector3> localPathPointList, Transform parentObject, LayerMask raycastLayers, bool subdivide)
        {
            if(subdivide)
            {
                for (int i = 0; i < localPathPointList.Count - 1; i+=2)
                {
                    Vector3 midPoint = (localPathPointList[i] + localPathPointList[i+1])/2;
                    localPathPointList.Insert(i+1, midPoint);
                }
            }

            var localToWorld = parentObject.localToWorldMatrix;

            bool pathRefined = true;
            while(pathRefined)
            {
                pathRefined = false;

                for(int i = 1; i < localPathPointList.Count - 2; i++)
                {
                    var firstPoint = localPathPointList[i];
                    var nextPoint = localPathPointList[i + 2];

                    if(!Physics2D.Linecast(localToWorld.MultiplyPoint(firstPoint), localToWorld.MultiplyPoint(nextPoint)))
                    {
                        // Remove the point in between if it can be bypassed
                        localPathPointList.RemoveAt(i + 1);
                        pathRefined = true;
                    }
                }
            }
        }

        private static PathfindingNode GetLowestCostNode(List<PathfindingNode> openList)
        {
            PathfindingNode lowestCostNode = null;
            float lowestCost = float.MaxValue;

            for(int i = 0; i < openList.Count; i++)
            {
                var currentNode = openList[i];
                
                if(currentNode.F < lowestCost)
                {
                    lowestCost = currentNode.F;
                    lowestCostNode = currentNode;
                }
            }
            return lowestCostNode;
        }

        private static void AddAdjacentToOpenList(PathfindingNode currentNode, List<PathfindingNode> openList, List<PathfindingNode> closedList, Vector3Int targetTilePosition, Tilemap obstacleTilemap)
        {
            bool down = AddAdjacentToOpenList(currentNode, openList, closedList, targetTilePosition, obstacleTilemap, 0, -1, 1);
            bool up = AddAdjacentToOpenList(currentNode, openList, closedList, targetTilePosition, obstacleTilemap, 0, 1, 1);
            bool left = AddAdjacentToOpenList(currentNode, openList, closedList, targetTilePosition, obstacleTilemap, -1, 0, 1);
            bool right = AddAdjacentToOpenList(currentNode, openList, closedList, targetTilePosition, obstacleTilemap, 1, 0, 1);

            if(left && down)
                AddAdjacentToOpenList(currentNode, openList, closedList, targetTilePosition, obstacleTilemap, -1, -1, 1.5f);
            if (right && down)
                AddAdjacentToOpenList(currentNode, openList, closedList, targetTilePosition, obstacleTilemap, 1, -1, 1.5f);
            if(left && up)
                AddAdjacentToOpenList(currentNode, openList, closedList, targetTilePosition, obstacleTilemap, -1, 1, 1.5f);
            if(right && up)
                AddAdjacentToOpenList(currentNode, openList, closedList, targetTilePosition, obstacleTilemap, 1, 1, 1.5f);
        }

        private static bool AddAdjacentToOpenList(PathfindingNode currentNode, List<PathfindingNode> openList, List<PathfindingNode> closedList, Vector3Int targetTilePosition, Tilemap obstacleTilemap, int offsetX, int offsetY, float moveCost)
        {
            var newPosition = currentNode.Position + new Vector3Int(offsetX, offsetY, 0);
            bool walkable = !obstacleTilemap.HasTile(newPosition);
            if (!walkable) return false;

            bool inClosedList = closedList.FindIndex(x => x.Position == newPosition) != -1;
            if (inClosedList) return false;

            PathfindingNode newNode = new PathfindingNode { Position = newPosition, G = currentNode.G + moveCost, Parent = currentNode };
            newNode.CalculateH(targetTilePosition);

            int openListIndex = openList.FindIndex(x => x.Position == newNode.Position);
            bool inOpenList = openListIndex != -1;

            if(inOpenList)
            {
                if (newNode.G < openList[openListIndex].G)
                {
                    openList[openListIndex] = newNode;
                }
            }
            else
            {
                openList.Add(newNode);
            }

            return true;
        }
    }
}