﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MossWolf.Utility.Math
{
    /// <summary>
    /// Used for collecting information needed for the particles in the Unity inspector.
    /// </summary>
    [System.Serializable]
    public class DebugParticleInfo
    {
        public bool DebugMode = false;
        public Material DebugParticleMaterial;
        public Color DebugParticleColor = new Color(1.0f, 0.5f, 0.0f);
        public float Size = 0.25f;
    }

    /// <summary>
    /// Creates a particle system used to spawn particles used for debugging (e.g. finding which path tiles are being checked for a pathfinder)
    /// </summary>
    public class DebugParticles
    {
        private ParticleSystem debugParticles;
        private Transform debugParticlesTransform;
        private ParticleSystem.EmissionModule debugParticleEmit;
        private DebugParticleInfo info;
        private Transform targetTransform;
        private bool attachToParent;

        public DebugParticles(DebugParticleInfo info, Transform targetTransform, bool attachToParent = true)
        {
            this.info = info;
            this.targetTransform = targetTransform;
            this.attachToParent = attachToParent;

            if (info.DebugMode)
            {
                var particleObject = new GameObject(string.Format("DebugParticles-{0}", targetTransform.name));
                debugParticlesTransform = particleObject.transform;
                if(attachToParent)
                    debugParticlesTransform.SetParent(targetTransform.parent, false);
                else
                    debugParticlesTransform.SetParent(targetTransform, false);
                debugParticlesTransform.localPosition = new Vector3(0, 0, -5);
                debugParticles = particleObject.AddComponent<ParticleSystem>();

                var debugParticleMain = debugParticles.main;
                debugParticleMain.startSize = info.Size;
                debugParticleMain.startLifetime = 0.25f;
                debugParticleMain.maxParticles = 1000;
                debugParticleMain.startColor = info.DebugParticleColor;

                debugParticleEmit = debugParticles.emission;
                debugParticleEmit.enabled = false;

                var particleRenderer = debugParticles.GetComponent<ParticleSystemRenderer>();
                particleRenderer.sortingLayerName = "Foreground";
                particleRenderer.sortingOrder = 1000;
                particleRenderer.sharedMaterial = info.DebugParticleMaterial;
            }
        }

        public void UpdateTransformParent()
        {
            if(attachToParent)
                debugParticlesTransform.SetParent(targetTransform.parent, false);
            else
                debugParticlesTransform.SetParent(targetTransform, false);
        }

        public void EmitAtLocation(Vector3 worldPosition)
        {
            debugParticles.Emit(new ParticleSystem.EmitParams { position = worldPosition }, 1);
        }

        public void EmitAtLocation(Vector3 worldPosition, Color color)
        {
            debugParticles.Emit(new ParticleSystem.EmitParams { position = worldPosition, startColor = color }, 1);
        }
    }
}