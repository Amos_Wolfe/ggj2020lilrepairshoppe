﻿namespace MossWolf.Base.Events
{
    /// <summary>
    /// Used for all global game events (game events about the overall game state).
    /// </summary>
    public class GlobalEvents : BaseBehaviour
    {
        public static EventBus Events = new EventBus();
    }
}