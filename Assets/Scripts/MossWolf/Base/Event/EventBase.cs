﻿namespace MossWolf.Base.Events
{
    public abstract class EventBase
    {
        public abstract void Reset();
    }
}