﻿using System.Collections.Generic;

namespace MossWolf.Base.Events
{
    public class EventBus
    {
        private Dictionary<System.Type, List<System.WeakReference>> allSubscribers = new Dictionary<System.Type, List<System.WeakReference>>();

        public void Publish<T>(T e) where T : EventBase
        {
            var subscriberType = typeof(ISubscriber<>).MakeGenericType(typeof(T));

            var subscriberlist = GetSubscribers(subscriberType);
            var subscribersToRemove = new List<System.WeakReference>();

            foreach(System.WeakReference weakSubscriber in subscriberlist)
            {
                if(weakSubscriber.IsAlive)
                {
                    var subscriber = (ISubscriber<T>)weakSubscriber.Target;
                    subscriber.OnEvent(e);
                }
                else
                {
                    subscribersToRemove.Add(weakSubscriber);
                }
            }

            foreach(System.WeakReference subscriber in subscribersToRemove)
            {
                subscriberlist.Remove(subscriber);
            }

            e.Reset();
        }

        public void Subscribe(object subscriber)
        {
            var allInterfaces = subscriber.GetType().GetInterfaces();

            var weakRef = new System.WeakReference(subscriber);

            foreach(System.Type subscriberType in allInterfaces)
            {
                if (!subscriberType.IsGenericType)
                    continue;
                if (subscriberType.GetGenericTypeDefinition() != typeof(ISubscriber<>))
                    continue;

                var subscriberlist = GetSubscribers(subscriberType);
                subscriberlist.Add(weakRef);
            }
        }

        public void UnSubScribe(object subscriber)
        {
            var allInterfaces = subscriber.GetType().GetInterfaces();

            var weakRef = new System.WeakReference(subscriber);

            foreach (System.Type subscriberType in allInterfaces)
            {
                if (!subscriberType.IsGenericType)
                    continue;
                if (subscriberType.GetGenericTypeDefinition() != typeof(ISubscriber<>))
                    continue;

                var subscriberlist = GetSubscribers(subscriberType);
                subscriberlist.Remove(weakRef);
            }
        }

        public void UnSubscribeAll()
        {
            allSubscribers.Clear();
        }

        private List<System.WeakReference> GetSubscribers(System.Type type)
        {
            List<System.WeakReference> subscribersList = null;
            bool found = allSubscribers.TryGetValue(type, out subscribersList);
            if(!found)
            {
                subscribersList = new List<System.WeakReference>();
                allSubscribers.Add(type, subscribersList);
            }

            return subscribersList;
        }
    }
}