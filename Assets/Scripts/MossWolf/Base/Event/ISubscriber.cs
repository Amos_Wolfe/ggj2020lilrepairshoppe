﻿namespace MossWolf.Base.Events
{
    public interface ISubscriber<T>
    {
        void OnEvent(T e);
    }
}