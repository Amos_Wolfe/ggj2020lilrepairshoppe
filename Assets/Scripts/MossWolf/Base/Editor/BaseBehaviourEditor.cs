﻿using MossWolf.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CanEditMultipleObjects]
[CustomEditor(typeof(BaseBehaviour), true)]
public class BaseBehaviourEditor : Editor
{
    public override void OnInspectorGUI()
    {
        var behaviour = target as BaseBehaviour;

        var helpText = behaviour.HelpText;
        if(!string.IsNullOrEmpty(helpText))
        {
            EditorGUILayout.HelpBox(helpText, MessageType.Info);
        }

        base.OnInspectorGUI();
    }
}
