﻿using System.Collections.Generic;
using UnityEngine;

namespace MossWolf.Base
{
	/// <summary>
	/// A global event manager that other scripts can publish events to and receive events from.
	/// Extend this class and define T as a type to define different game events (an enum is best for this).
	/// </summary>
	public abstract class GameEventsBase<T> : BaseBehaviour
	{

		#region Fields
		private Dictionary<GameEvent, List<IGameEventReceiver>> gameEventTable;
		#endregion

		#region Unity Methods
		private void Awake()
		{
			InitialiseReceiverTable();
		}
		#endregion

		#region Public API
		public bool Subscribe(GameEvent gameEvent, IGameEventReceiver receiver)
		{
			InitialiseReceiverTable();

			var receiverList = gameEventTable[gameEvent];
			if (receiverList.Contains(receiver))
				return false;

			receiverList.Add(receiver);
			return true;
		}

		public bool UnSubscribe(GameEvent gameEvent, IGameEventReceiver receiver)
		{
			InitialiseReceiverTable();

			var receiverList = gameEventTable[gameEvent];
			return receiverList.Remove(receiver);
		}

		public bool UnSubscribeAll(IGameEventReceiver receiver)
		{
			InitialiseReceiverTable();

			bool removedFromGameEvent = false;
			foreach (var gameEvent in gameEventTable.Keys)
			{
				if (UnSubscribe(gameEvent, receiver))
					removedFromGameEvent = true;
			}
			return removedFromGameEvent;
		}

		public void Publish(GameEvent gameEvent, object args = null)
		{
			InitialiseReceiverTable();

			foreach (var reciever in gameEventTable[gameEvent])
			{
				try
				{
					reciever.OnGameEvent(gameEvent, args);
				}
				catch (System.Exception e)
				{
					LogError("Exception while running game event {0} on receiver {1}. Message: {2}. Stack Trace: {3}", gameEvent, reciever.Name, e.Message, e.StackTrace);
				}
			}
		}
		#endregion

		#region Private Methods
		private void InitialiseReceiverTable()
		{
			if (gameEventTable != null)
				return;
			gameEventTable = new Dictionary<GameEvent, List<IGameEventReceiver>>();

			var allGameEvents = System.Enum.GetValues(typeof(GameEvent)) as GameEvent[];

			foreach (var gameEvent in allGameEvents)
				gameEventTable.Add(gameEvent, new List<IGameEventReceiver>());
		}
		#endregion

	}
}