﻿using MossWolf.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameEvent
{
	
}

/// <summary>
/// A global event manager that other scripts can publish events to and receive events from.
/// </summary
public class GameEvents : GameEventsBase<GameEvent> { }
