﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MossWolf.Base
{
	/// <summary>
	/// A BaseBehaviour that can receive and sent game events.
	/// </summary>
	public abstract class EventBehaviour : BaseBehaviour, IGameEventReceiver
	{
		protected GameEvents events;

		protected override void AwakeEx()
		{
			base.AwakeEx();

			ReferenceManager.GetBehaviours(ref events);
		}

		private void OnDestroy()
		{
			if(events != null)
				events.UnSubscribeAll(this);
		}

		#region IGameEventReceiver
		public virtual string Name
		{
			get
			{
				if (this == null || this.gameObject == null)
					return "Null";
				return gameObject.name;
			}
		}

		public virtual void OnGameEvent(GameEvent eventType, object args)
		{
			if (this == null || this.gameObject == null)
			{
				events.UnSubscribeAll(this);
				return;
			}
		}
		#endregion
	}
}