﻿using UnityEngine;
using System.Collections;

namespace MossWolf.Base
{
	/// <summary>
	/// Receives global game events
	/// </summary>
	public interface IGameEventReceiver
	{
		/// <summary>
		/// The name of this receiver (useful for error logging).
		/// </summary>
		string Name { get; }

		void OnGameEvent(GameEvent gameEvent, object args);
	}
}