﻿using UnityEngine;
using System.Collections;
using System.Reflection;
using MossWolf.Utility.GameObjectUtil;

namespace MossWolf.Base
{
	/// <summary>
	/// All classes extend from this instead of MonoBehaviour. Adds extra convenience methods
	/// </summary>
	public abstract class BaseBehaviour : MonoBehaviour
	{

		private bool awakeExCalled;
        protected Transform cachedTransform;

        /// <summary>
        /// Set this to show help text about this component in the Unity inspector.
        /// </summary>
        public virtual string HelpText { get; }

        /// <summary>
        /// Returns the transform of this object. Faster than calling transform.
        /// </summary>
        public Transform CachedTransform { get { return cachedTransform; } }

		private void Awake()
		{
			CallAwakeEx();
		}

		/// <summary>
		/// Used instead of awake. 
		/// </summary>
		protected void CallAwakeEx()
		{
			if (awakeExCalled) return;
			awakeExCalled = true;

			AwakeEx();
		}

		/// <summary>
		/// Used instead of awake. Can be called before awake with CallAwakeEx(). This ensures variables are initialised.
		/// </summary>
		protected virtual void AwakeEx()
        {
            cachedTransform = transform;
        }

		protected bool CheckIfSet(params Component[] targetField)
		{
			for (int i = 0; i < targetField.Length; i++)
			{
				if (targetField[i] == null)
				{
					Debug.LogErrorFormat("[{0}, name:{1}] {2} not set!", GetType(), name, targetField[i].GetType().Name);
					this.enabled = false;
					return false;
				}
			}

			return true;
		}

		protected bool CheckIfSet<T>(T targetObject)
		{
			if (targetObject == null)
			{
				Debug.LogErrorFormat("[{0}, name:{1}] {2} not set!", GetType(), name, typeof(T).Name);
				this.enabled = false;
				return false;
			}

			return true;
		}

		protected bool CheckIfSet<T, U>(T targetObject1, U targetObject2)
		{
			if (!CheckIfSet(targetObject1)) return false;
			if (!CheckIfSet(targetObject2)) return false;

			return true;
		}

		protected bool GetComponentEx<T>(ref T targetField, bool inParent = false)
			where T : Object
		{
			if (!inParent)
				targetField = GetComponent<T>();
			else
				targetField = GetComponentInParent<T>();
			if (targetField == null)
			{
				Debug.LogErrorFormat("[{0}, name:{1}] {2} not found!", this.GetType(), this.name, typeof(T).Name);
				this.enabled = false;
				return false;
			}

			return true;
		}

		protected bool GetComponentEx<T, U>(ref T targetField1, ref U targetField2, bool inParent = false)
			where T : Component
			where U : Component
		{
			if (!GetComponentEx(ref targetField1, inParent))
				return false;
			if (!GetComponentEx(ref targetField2, inParent))
				return false;

			return true;
		}

        protected bool GetComponentEx<T, U, V>(ref T targetField1, ref U targetField2, ref V targetField3, bool inParent = false)
            where T : Component
            where U : Component
            where V : Component
        {
            if (!GetComponentEx(ref targetField1, inParent))
                return false;
            if (!GetComponentEx(ref targetField2, inParent))
                return false;
            if (!GetComponentEx(ref targetField3, inParent))
                return false;

            return true;
        }

        protected bool GetComponentEx<T, U, V, W>(ref T targetField1, ref U targetField2, ref V targetField3, ref W targetField4, bool inParent = false)
            where T : Component
            where U : Component
            where V : Component
            where W : Component
        {
            if (!GetComponentEx(ref targetField1, inParent))
                return false;
            if (!GetComponentEx(ref targetField2, inParent))
                return false;
            if (!GetComponentEx(ref targetField3, inParent))
                return false;
            if (!GetComponentEx(ref targetField4, inParent))
                return false;

            return true;
        }

        // TODO ReferenceManager convenience methods

        protected void LogError(string errorString, params object[] args)
		{
			Debug.LogErrorFormat("[{0}, name:{1}] {2}", this.GetType(), GameObjectUtils.GetObjectPath(gameObject), string.Format(errorString, args));
		}

		protected void LogWarning(string errorString, params object[] args)
		{
			Debug.LogWarningFormat("[{0}, name:{1}] {2}", this.GetType(), GameObjectUtils.GetObjectPath(gameObject), string.Format(errorString, args));
		}

		protected void Log(string errorString, params object[] args)
		{
			Debug.LogFormat("[{0}, name:{1}] {2}", this.GetType(), GameObjectUtils.GetObjectPath(gameObject), string.Format(errorString, args));
		}

	}
}