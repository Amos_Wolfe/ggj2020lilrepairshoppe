﻿using UnityEngine;

namespace MossWolf.Base.Pooling
{
	public class PoolableObjectBase : EventBehaviour, IPoolableObject
	{

		protected PoolManager poolManager;

		protected bool spawned;

		protected override void AwakeEx()
		{
			base.AwakeEx();

			ReferenceManager.GetBehaviours(ref poolManager);
		}

		protected void DespawnSelf()
		{
			if (!spawned) return;

			if (poolManager != null)
				poolManager.Despawn(this);
		}

		#region IPoolableObject
		GameObject IPoolableObject.GameObject
		{
			get
			{
				return this.gameObject;
			}
		}

		int IPoolableObject.PoolID { get; set; }

        IPoolableObject[] IPoolableObject.ObjectCollection { get; set; }

        public virtual void Spawn()
		{
			spawned = true;
		}
		public virtual void Despawn()
		{
			spawned = false;
		}
        #endregion
    }
}