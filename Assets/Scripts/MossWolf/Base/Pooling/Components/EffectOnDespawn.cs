﻿using UnityEngine;

namespace MossWolf.Base.Pooling
{
    public class EffectOnDespawn : PoolableObjectBase
    {
        [SerializeField]
        private PoolableObjectBase hitEffect;

        public override void Despawn()
        {
            base.Despawn();

            poolManager.Spawn(hitEffect, transform.position, Quaternion.identity);
        }
    }
}