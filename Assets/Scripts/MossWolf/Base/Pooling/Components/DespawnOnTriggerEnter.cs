﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MossWolf.Base.Pooling
{
    public class DespawnOnTriggerEnter : PoolableObjectBase
    {
        private void OnTriggerEnter(Collider other)
        {
            if (other.isTrigger) return;
            DespawnSelf();
        }
    }
}