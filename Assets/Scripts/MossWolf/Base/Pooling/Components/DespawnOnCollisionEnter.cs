﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MossWolf.Base.Pooling
{
    public class DespawnOnCollisionEnter : PoolableObjectBase
    {
        private void OnCollisionEnter(Collision collision)
        {
            if (collision.collider.isTrigger) return;
            DespawnSelf();
        }
    }
}