﻿using UnityEngine;

namespace MossWolf.Base.Pooling
{
    public class PoolableTimedObject : PoolableObjectBase
    {
        [SerializeField]
        private float despawnTime = 1;

        private float despawnTimeLeft;

        public override void Spawn()
        {
            base.Spawn();

            despawnTimeLeft = despawnTime;
        }

        private void Update()
        {
            despawnTimeLeft -= Time.deltaTime;
            if (despawnTimeLeft < 0)
                DespawnSelf();
        }
    }
}