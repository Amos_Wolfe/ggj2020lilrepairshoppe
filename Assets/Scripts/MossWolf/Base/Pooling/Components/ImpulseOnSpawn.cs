﻿using UnityEngine;

namespace MossWolf.Base.Pooling
{
    public class ImpulseOnSpawn : PoolableObjectBase
    {
        [SerializeField]
        private float speed = 10;

        protected Rigidbody thisBody;

        protected override void AwakeEx()
        {
            base.AwakeEx();

            GetComponentEx(ref thisBody);
        }

        public override void Spawn()
        {
            base.Spawn();

            if (thisBody != null)
            {
                thisBody.AddForce(transform.right * speed, ForceMode.VelocityChange);
            }
        }

        public override void Despawn()
        {
            base.Despawn();
            StopVelocity();
        }

        private void StopVelocity()
        {
            if (thisBody != null)
            {
                thisBody.velocity = Vector3.zero;
                thisBody.angularVelocity = Vector3.zero;
            }
        }
    }
}