﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MossWolf.Base.Pooling
{
    /// <summary>
    /// Instantiates a pool of components, which can be activated and deactivated.
    /// </summary>
    public class ComponentPool<T> where T : Component
    {
        private T template;
        private Stack<T> pool;

        public ComponentPool(T template, int initialSize)
        {
            this.template = template;

            pool = new Stack<T>();

            for (int i = 0; i < initialSize; i++)
                InstantiateNewComponent();
        }

        public T Fetch()
        {
            if (pool.Count == 0)
                InstantiateNewComponent();

            T nextComponent = pool.Pop();
            nextComponent.gameObject.SetActive(true);
            return nextComponent;
        }

        public void Release(T currentComponent)
        {
            currentComponent.gameObject.SetActive(false);
            pool.Push(currentComponent);
        }

        private void InstantiateNewComponent()
        {
            T newComponent = GameObject.Instantiate(template);
            newComponent.gameObject.SetActive(false);
            pool.Push(newComponent);
        }
    }
}