﻿using System.Collections.Generic;
using UnityEngine;

namespace MossWolf.Base.Pooling
{

	[System.Serializable]
	public class PoolObjectInfo
	{
		public int InitialCount;
		public GameObject GameObject;
	}

	public class PoolManager : BaseBehaviour
	{
		[SerializeField]
		private List<PoolObjectInfo> poolTemplates;

		private Dictionary<string, int> poolIDReferenceTable;
		private Dictionary<int, IPoolableObject> templateTable;
		private Dictionary<int, Stack<IPoolableObject[]>> poolTable;
		private Dictionary<int, List<IPoolableObject[]>> spawnedTable;

		protected override void AwakeEx()
		{
			base.AwakeEx();

			poolIDReferenceTable = new Dictionary<string, int>();
			templateTable = new Dictionary<int, IPoolableObject>();
			poolTable = new Dictionary<int, Stack<IPoolableObject[]>>();
			spawnedTable = new Dictionary<int, List<IPoolableObject[]>>();

			for (int i = 0; i < poolTemplates.Count; i++)
			{
				var template = poolTemplates[i];
				if (template.GameObject == null)
				{
					LogError("Template GameObject is null");
					continue;
				}
				if (poolIDReferenceTable.ContainsKey(template.GameObject.name))
				{
					LogError("Duplicate GameObject name {0} found", template.GameObject.name);
					continue;
				}

				var poolable = template.GameObject.GetComponent<IPoolableObject>();
				if(poolable == null)
				{
					LogError("GameObject {0} has no IPoolableObject on it", template.GameObject.name);
					continue;
				}

				poolIDReferenceTable.Add(template.GameObject.name, i);

				var newPoolStack = new Stack<IPoolableObject[]>();
				poolTable.Add(i, newPoolStack);
				spawnedTable.Add(i, new List<IPoolableObject[]>());
				templateTable.Add(i, poolable);

				for (int poolindex = 0; poolindex < template.InitialCount; poolindex++)
				{
					AddToPool(newPoolStack, i);
				}
			}
		}

		public GameObject Spawn(IPoolableObject templateObject, Vector3 position, Quaternion rotation)
		{
			if (templateObject == null) return null;
			if (!poolIDReferenceTable.ContainsKey(templateObject.GameObject.name))
			{
				LogError("Object {0} not found", templateObject.GameObject.name);
				return null;
			}
			int poolID = poolIDReferenceTable[templateObject.GameObject.name];

			var poolStack = poolTable[poolID];

			if(poolStack.Count == 0)
			{
				AddToPool(poolStack, poolID);
			}

			IPoolableObject[] nextObject = poolStack.Pop();
			spawnedTable[poolID].Add(nextObject);

			nextObject[0].GameObject.transform.position = position;
			nextObject[0].GameObject.transform.rotation = rotation;
			nextObject[0].GameObject.SetActive(true);
			
            for(int i = 0; i < nextObject.Length; i++)
                nextObject[i].Spawn();

			return nextObject[0].GameObject;
		}

		public void Despawn(IPoolableObject spawnedObject)
		{
			if (spawnedObject == null) return;
			if(!poolIDReferenceTable.ContainsValue(spawnedObject.PoolID))
			{
				LogError("Object ID {0} not found for gameobject {1}", spawnedObject.PoolID, spawnedObject.GameObject.name);
				return;
			}

			int poolID = spawnedObject.PoolID;

			spawnedTable[poolID].Remove(spawnedObject.ObjectCollection);
			var poolStack = poolTable[poolID];
			poolStack.Push(spawnedObject.ObjectCollection);

            for (int i = 0; i < spawnedObject.ObjectCollection.Length; i++)
                spawnedObject.ObjectCollection[i].Despawn();

			spawnedObject.GameObject.SetActive(false);
		}

		private void AddToPool(Stack<IPoolableObject[]> poolStack, int poolID)
		{
			var template = templateTable[poolID];

			GameObject newObject = Instantiate(template.GameObject);
			var poolable = newObject.GetComponentsInChildren<IPoolableObject>();
            for(int i = 0; i < poolable.Length; i++)
            {
                poolable[i].PoolID = poolID;
                poolable[i].ObjectCollection = poolable;
            }
			newObject.SetActive(false);
			poolStack.Push(poolable);
		}
	}
}