﻿using UnityEngine;

namespace MossWolf.Base.Pooling
{
	public interface IPoolableObject
	{
		GameObject GameObject { get; }
		int PoolID { get; set; }
        /// <summary>
        /// All the poolable components on this object
        /// </summary>
        IPoolableObject[] ObjectCollection { get; set; }

		void Spawn();
		void Despawn();
	}
}