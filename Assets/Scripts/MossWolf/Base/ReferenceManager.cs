﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

namespace MossWolf.Base
{
	/// <summary>
	/// Gets references to single class objects, and returns errors if they don't exist
	/// </summary>
	public class ReferenceManager
	{
        private static Dictionary<int, Object> referenceTable;

        public static void ClearReferences()
        {
            if (referenceTable != null)
                referenceTable.Clear();
        }

		public static void GetBehaviours<T>(ref T b1)
			where T : Object
		{
			GetBehaviour(ref b1);
		}
        
		public static void GetBehaviours<T, U>(ref T b1, ref U b2)
			where T : Object
			where U : Object
		{
			GetBehaviour(ref b1);
			GetBehaviour(ref b2);
		}

		public static void GetBehaviours<T, U, V>(ref T b1, ref U b2, ref V b3)
			where T : Object
			where U : Object
			where V : Object
		{
			GetBehaviour(ref b1);
			GetBehaviour(ref b2);
			GetBehaviour(ref b3);
		}

		private static void GetBehaviour<T>(ref T b) where T : Object
		{
            if (referenceTable == null)
                referenceTable = new Dictionary<int, Object>();

            int typeInt = typeof(T).GetHashCode();
            if (referenceTable.ContainsKey(typeInt))
                b = referenceTable[typeInt] as T;

            if (b == null)
            {
                b = Object.FindObjectOfType<T>();
                referenceTable[typeInt] = b;
            }

			if (b == null)
				Debug.LogErrorFormat("[ReferenceManager] Could not find object of type {0}", typeof(T).Name);
		}
	}
}