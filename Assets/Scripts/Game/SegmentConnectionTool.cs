﻿using MossWolf.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Automatically pulls the correct segments to the one being held. Connects segments together to create an item.
/// </summary>
public class SegmentConnectionTool : BaseBehaviour
{
    private const float HOVER_HEIGHT = 1.5f;

    
    private const float MOVE_FORCE = 2;
    private const float MAX_FORCE = 5;
    private const float DAMPENING = 0.4f;

    private const float MAGNET_DISTANCE = 1.75f;

    /// <summary>
    /// If a segment is this close to the terrain, it will not rotate.
    /// </summary>
    private const float ROTATION_FREE_ZONE = 0.2f;

    private string SEGMENT_LAYER = "Segment";

    [SerializeField]
    private LayerMask hoverRaycastLayer = (1 << 12);

    private SegmentAttacher segmentAttacher;

    private SegmentConnectionLine connectionLine;

    private Camera mainCamera;
    private Transform cameraTransform;
    private Segment activeSegment;
    private bool hovered;
    private bool dragged;
    private int segmentLayer;

    private Plane movePlane;
    private Vector3 startSegmentPosition;
    private Vector3 startMouseWorldPosition;
    private Vector3 lastMousePosition;
    private Vector3 grabLocalOffset;
    private Collider[] colliderList;

    private bool connectNextFrame;
    private AttachmentPoint firstAttachment;
    private AttachmentPoint secondAttachment;

    private List<Rigidbody> currentNoGravityBodies;
    private List<Rigidbody> newNoGravityBodies;

    protected override void AwakeEx()
    {
        base.AwakeEx();
        
        ReferenceManager.GetBehaviours(ref segmentAttacher);

        connectionLine = GetComponentInChildren<SegmentConnectionLine>(true);

        colliderList = new Collider[100];

        currentNoGravityBodies = new List<Rigidbody>();
        newNoGravityBodies = new List<Rigidbody>();

        mainCamera = Camera.main;
        cameraTransform = mainCamera.transform;
        segmentLayer = LayerMask.GetMask(SEGMENT_LAYER);
    }

    private void Update()
    {
        if (!dragged)
        {
            Ray mouseRay = mainCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(mouseRay, out hit, 1000, segmentLayer, QueryTriggerInteraction.Collide))
            {
                if (activeSegment != null)
                    activeSegment.OnCollide -= OnCollide;

                activeSegment = hit.transform.GetComponentInParent<Segment>();

                if (activeSegment != null)
                    activeSegment.OnCollide += OnCollide;

                hovered = activeSegment != null;
            }
            else
            {
                hovered = false;
                activeSegment = null;
            }
        }

        if (dragged || hovered)
        {
            if (Input.GetMouseButtonDown(0))
            {
                dragged = true;
                OnBeginDrag();
            }
            else if (Input.GetMouseButtonUp(0))
            {
                if (dragged)
                {
                    dragged = false;
                    OnEndDrag();
                }
            }
        }
    }

    private void FixedUpdate()
    {
        if (dragged)
            OnDrag();
    }

    private void OnCollide(Collision collision)
    {
        if (!dragged)
            return;
        if (connectNextFrame || segmentAttacher.Busy)
            return;

        Segment segment = collision.collider.GetComponentInParent<Segment>();
        if (segment == null)
            return;

        if (FindConnectingAttachments(segment, out firstAttachment, out secondAttachment))
        {
            Vector3 offset = firstAttachment.CachedTransform.position - secondAttachment.CachedTransform.position;

            //if(offset.magnitude < CONNECT_PROXIMITY)
            connectNextFrame = true;
        }
    }

    private void OnBeginDrag()
    {
        connectionLine.ConnectTo(activeSegment.CachedTransform);

        movePlane = new Plane(Vector3.up, activeSegment.CachedTransform.position);
        startSegmentPosition = activeSegment.CachedTransform.position + Vector3.up * HOVER_HEIGHT;
        startMouseWorldPosition = GetMouseFlatPosition();
        grabLocalOffset = activeSegment.CachedTransform.worldToLocalMatrix.MultiplyPoint(GetMouseSegmentPosition());
        lastMousePosition = Input.mousePosition;

        if (activeSegment.ParentItem.RepairPercentage == 1)
            activeSegment.ParentItem.DoPickupEffects();
    }

    private void OnDrag()
    {
        Vector3 newMousePos = GetMouseFlatPosition();
        Vector3 moveOffset = newMousePos - startMouseWorldPosition;
        Vector3 targetPosition = startSegmentPosition + moveOffset;

        RaycastHit hit;
        if(Physics.Raycast(targetPosition, -Vector3.up, out hit, 100, hoverRaycastLayer))
        {
            targetPosition = hit.point + Vector3.up * HOVER_HEIGHT;
        }

        MoveTo(targetPosition);

        AutoDetectNearbySegments();

        if (connectNextFrame)
        {
            connectNextFrame = false;
            if (firstAttachment != null && secondAttachment != null)
                firstAttachment.TryToAttach(secondAttachment);
        }
    }

    private void OnEndDrag()
    {
        connectNextFrame = false;
        connectionLine.Disconnect();
    }

    private void AutoDetectNearbySegments()
    {
        if (connectNextFrame || segmentAttacher.Busy)
            return;

        Vector3 currentPosition = activeSegment.CachedTransform.position;

        // Find all segments in range
        int colliderCount = Physics.OverlapSphereNonAlloc(activeSegment.CachedTransform.position, MAGNET_DISTANCE, colliderList, segmentLayer);

        activeSegment.Rigidbody.AddTorque(-activeSegment.Rigidbody.angularVelocity * 0.3f, ForceMode.Acceleration);

        newNoGravityBodies.Clear();

        for (int i = 0; i < colliderCount; i++)
        {
            Segment segment = colliderList[i].GetComponentInParent<Segment>();
            if (segment == null)
                continue;

            AttachmentPoint firstAttachment, secondAttachment;
            if (FindConnectingAttachments(segment, out firstAttachment, out secondAttachment))
            {
                Vector3 offsetForce = firstAttachment.CachedTransform.position - secondAttachment.CachedTransform.position;

                float distanceFactor = Mathf.Clamp01(1 - (offsetForce.magnitude / MAGNET_DISTANCE));
                offsetForce.Normalize();
                offsetForce *= distanceFactor;

                Rigidbody targetBody = secondAttachment.ParentSegment.Rigidbody;
                targetBody.AddForce(offsetForce * 0.4f + (-targetBody.velocity * 0.3f) * distanceFactor, ForceMode.VelocityChange);

                float rotateForce = 0.15f * distanceFactor;
                RotateSegmentTowards(firstAttachment.CachedTransform.rotation, secondAttachment.CachedTransform.rotation, targetBody, rotateForce);
                RotateSegmentTowards(Quaternion.LookRotation(firstAttachment.CachedTransform.position - secondAttachment.CachedTransform.position), firstAttachment.CachedTransform.rotation, activeSegment.Rigidbody, rotateForce);

                newNoGravityBodies.Add(segment.Rigidbody);
            }
        }

        for(int  i = 0; i < currentNoGravityBodies.Count; i++)
        {
            Rigidbody currentBody = currentNoGravityBodies[i];
            if(!newNoGravityBodies.Contains(currentBody))
            {
                currentBody.useGravity = true;
                currentNoGravityBodies.RemoveAt(i);
                --i;
            }
        }

        for(int i = 0; i < newNoGravityBodies.Count; i++)
        {
            Rigidbody newBody = newNoGravityBodies[i];
            if(!currentNoGravityBodies.Contains(newBody))
            {
                newBody.useGravity = false;
                currentNoGravityBodies.Add(newBody);
            }
        }
    }

    private void RotateSegmentTowards(Quaternion targetRotation, Quaternion currentRotation, Rigidbody targetBody, float amount)
    {
        if (Physics.CheckSphere(targetBody.position, ROTATION_FREE_ZONE, hoverRaycastLayer))
            return;

        Quaternion offset = targetRotation * (Quaternion.Euler(180, 0, 0) * Quaternion.Inverse(currentRotation));
        targetBody.MoveRotation(Quaternion.Slerp(targetBody.rotation, offset * targetBody.rotation, amount));
    }

    private bool FindConnectingAttachments(Segment otherSegment, out AttachmentPoint firstAttachment, out AttachmentPoint secondAttachment)
    {
        if (otherSegment.ParentItem != activeSegment.ParentItem)
        {
            firstAttachment = null;
            secondAttachment = null;
            return false;
        }

        List<Segment> firstSegmentList = new List<Segment>();
        activeSegment.GetAttachedSegments(activeSegment, firstSegmentList);

        List<Segment> secondSegmentList = new List<Segment>();
        activeSegment.GetAttachedSegments(otherSegment, secondSegmentList);

        foreach (Segment segment1 in firstSegmentList)
        {
            foreach (Segment segment2 in secondSegmentList)
            {
                // See if any attachment points connect
                foreach (AttachmentPoint attachment1 in segment1.AttachmentPoints)
                {
                    if (attachment1.AttachedTo != null)
                        continue;

                    foreach (AttachmentPoint attachment2 in segment2.AttachmentPoints)
                    {
                        if (attachment2.AttachedTo != null)
                            continue;

                        if (attachment1.CorrectAttachment == attachment2)
                        {
                            firstAttachment = attachment1;
                            secondAttachment = attachment2;
                            return true;
                        }
                    }
                }
            }
        }

        firstAttachment = null;
        secondAttachment = null;
        return false;
    }

    private Vector3 GetMouseFlatPosition()
    {
        Ray mouseRay = mainCamera.ScreenPointToRay(Input.mousePosition);
        float distance;
        movePlane.Raycast(mouseRay, out distance);
        return mouseRay.GetPoint(distance);
    }

    private Vector3 GetMouseSegmentPosition()
    {
        Ray mouseRay = mainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(mouseRay, out hit))
            return hit.point;

        return Vector3.zero;
    }

    private void MoveTo(Vector3 targetPosition)
    {
        // NOTE: not using RigidBody.MovePosition() as it can push objects through each other
        Vector3 pushForce = targetPosition - activeSegment.Rigidbody.position;
        Vector3 grabWorldOffset = activeSegment.CachedTransform.localToWorldMatrix.MultiplyPoint(grabLocalOffset);
        pushForce *= MOVE_FORCE;

        if (pushForce.magnitude > MAX_FORCE)
        {
            pushForce.Normalize();
            pushForce *= MAX_FORCE;
        }

        activeSegment.Rigidbody.AddForceAtPosition(pushForce - activeSegment.Rigidbody.velocity * DAMPENING, grabWorldOffset, ForceMode.VelocityChange);
    }
}
