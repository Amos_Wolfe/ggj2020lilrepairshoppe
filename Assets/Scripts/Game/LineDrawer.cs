﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineDrawer
{
    private Texture2D lineTexture;

    public LineDrawer(Texture2D lineTexture)
    {
        this.lineTexture = lineTexture;
    }

    public void Draw(Vector2 start, Vector2 end, float lineWidth)
    {
        Vector2 offset = end - start;

        float length = offset.magnitude;
        Rect rect = new Rect(start.x, start.y - lineWidth / 2, length, lineWidth);

        float rotationAngle = Mathf.Atan2(offset.y, offset.x) * Mathf.Rad2Deg;
        GUIUtility.RotateAroundPivot(rotationAngle, start);
        GUI.DrawTexture(rect, lineTexture);
        GUIUtility.RotateAroundPivot(-rotationAngle, start);
    }

    public void DrawWorldPosition(Vector3 start, Vector3 end, float lineWidth)
    {
        Camera mainCamera = Camera.main;
        Vector2 startScreen = mainCamera.WorldToScreenPoint(start);
        Vector2 endScreen = mainCamera.WorldToScreenPoint(end);
        Draw(startScreen, endScreen, lineWidth);
    }
}
