﻿using MossWolf.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class Segment : BaseBehaviour
{
    private const float MAX_DISTANCE = 30;
    
    private Rigidbody thisBody;
    public Rigidbody Rigidbody => thisBody;
    
    public List<AttachmentPoint> AttachmentPoints { get; private set; }

    public List<Collider> Colliders { get; private set; }

    /// <summary>
    /// A value from 0 to 1, where 0 is completely unfixed and 1 is completly fixed
    /// </summary>
    public float RepairPercentage
    {
        get
        {
            if (AttachmentPoints.Count == 0)
                return 0;

            int attachmentsRepaired = 0;

            for (int i = 0; i < AttachmentPoints.Count; i++)
            {
                AttachmentPoint currentAttachment = AttachmentPoints[i];
                if (currentAttachment.AttachedTo == currentAttachment.CorrectAttachment)
                    attachmentsRepaired++;
            }

            return Mathf.Clamp01((float)attachmentsRepaired / (float)AttachmentPoints.Count);
        }
    }

    public Item ParentItem { get; private set; }

    public event UnityAction<Collision> OnCollide;

    protected override void AwakeEx()
    {
        base.AwakeEx();
        
        GetComponentEx(ref thisBody);

        if(AttachmentPoints == null)
            AttachmentPoints = new List<AttachmentPoint>();

        

        Colliders = new List<Collider>(GetComponentsInChildren<Collider>(true));
        
        ParentItem = GetComponentInParent<Item>();

        RefreshAttachmentPoints();
    }
    
    private void FixedUpdate()
    {
        // Reset the position if this segment goes outside the play area
        if (cachedTransform.position.magnitude > MAX_DISTANCE)
        {
            cachedTransform.position = Vector3.up * 3;
            Rigidbody.velocity = Vector3.zero;
        }
    }

    public void RefreshAttachmentPoints()
    {
        AttachmentPoints = new List<AttachmentPoint>(GetComponentsInChildren<AttachmentPoint>());
    }
    
    private void OnCollisionStay(Collision collision)
    {
        OnCollide?.Invoke(collision);
        
    }
    
    /// <summary>
    /// Finds all segments attached to this segment, including this segment.
    /// </summary>
    /// <returns>A list of segments</returns>
    public void GetAttachedSegments(Segment currentSegment, List<Segment> segmentList)
    {
        if (segmentList.Contains(currentSegment))
            return;

        segmentList.Add(currentSegment);

        foreach (AttachmentPoint attachment1 in currentSegment.AttachmentPoints)
        {
            if (attachment1.AttachedTo != null)
            {
                Segment newSegment = attachment1.AttachedTo.ParentSegment;
                GetAttachedSegments(newSegment, segmentList);
            }
        }
    }

    
}
