﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Used for animation events
/// </summary>
public class DeactivateObject : MonoBehaviour
{
    public void Deactivate()
    {
        gameObject.SetActive(false);
    }
}
