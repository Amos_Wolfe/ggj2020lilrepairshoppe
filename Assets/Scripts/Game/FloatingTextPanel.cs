﻿using MossWolf.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FloatingTextPanel : BaseBehaviour
{
    [SerializeField]
    private GameObject floatingTextTemplate;

    private Camera mainCamera;

    protected override void AwakeEx()
    {
        base.AwakeEx();

        mainCamera = Camera.main;

        floatingTextTemplate.gameObject.SetActive(false);
    }

    public void ShowFloatingText(string text, Vector3 worldPosition)
    {
        Vector3 screenPosition = mainCamera.WorldToScreenPoint(worldPosition);
        screenPosition.z = 0;

        GameObject newObject = Instantiate(floatingTextTemplate, transform);
        newObject.transform.position = screenPosition;
        Text floatingText = newObject.GetComponentInChildren<Text>();
        floatingText.text = text;
        newObject.SetActive(true);
    }
}
