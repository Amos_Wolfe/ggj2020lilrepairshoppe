﻿using MossWolf.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SellEffects
{
    public abstract class SellEffectBase : BaseBehaviour
    {
        protected ScoreManager scoreManager;

        protected override void AwakeEx()
        {
            base.AwakeEx();

            ReferenceManager.GetBehaviours(ref scoreManager);
        }

        public IEnumerator _RunSellEffect(ItemSellInfo info)
        {
            scoreManager.PauseSellingItems = true;

            yield return _RunEffect(info);

            scoreManager.PauseSellingItems = false;
        }

        protected abstract IEnumerator _RunEffect(ItemSellInfo info);
    }
}