﻿using MossWolf.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SellEffects
{
    public class ShowFloatingText : SellEffectBase
    {
        [SerializeField]
        private string text;
        [SerializeField]
        private float offsetRandomisation = 0;

        private FloatingTextPanel floatingTextPanel;

        protected override void AwakeEx()
        {
            base.AwakeEx();

            ReferenceManager.GetBehaviours(ref floatingTextPanel);
        }

        protected override IEnumerator _RunEffect(ItemSellInfo info)
        {
            Vector3 position = info.Item.AveragePosition;
            position += UnityEngine.Random.insideUnitSphere * offsetRandomisation;
            floatingTextPanel.ShowFloatingText(text, position);
            yield break;
        }
    }
}