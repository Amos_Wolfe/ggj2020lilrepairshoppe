﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SellEffects
{
    public class Wait : SellEffectBase
    {
        [SerializeField]
        private float seconds = 1;

        protected override IEnumerator _RunEffect(ItemSellInfo info)
        {
            yield return new WaitForSeconds(seconds);
        }
    }
}