﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SellEffects
{
    public class PlaySound : SellEffectBase
    {
        private AudioSource thisAudio;

        protected override void AwakeEx()
        {
            base.AwakeEx();
            
            GetComponentEx(ref thisAudio);
        }

        protected override IEnumerator _RunEffect(ItemSellInfo info)
        {
            thisAudio.Play();
            yield break;
        }
    }
}