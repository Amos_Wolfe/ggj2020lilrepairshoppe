﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SellEffects
{
    public class AddScore : SellEffectBase
    {
        [SerializeField]
        private float scorePenalty = -2;

        protected override IEnumerator _RunEffect(ItemSellInfo info)
        {
            scoreManager.AddScore(scorePenalty, info.Item.AveragePosition);
            yield break;
        }
    }
}