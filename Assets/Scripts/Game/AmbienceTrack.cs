﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbienceTrack : MonoBehaviour
{
    private static AmbienceTrack _instance;

    private void Awake()
    {
        if(_instance != null)
        {
            Destroy(gameObject);
            return;
        }

        _instance = this;
        transform.SetParent(null);
        DontDestroyOnLoad(this);
    }
}
