﻿using MossWolf.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Connects a bending beam to the held segment.
/// </summary>
public class SegmentConnectionLine : BaseBehaviour
{
    private const string MAT_PROPERTY = "_Color";

    [SerializeField]
    private int segments = 16;
    [SerializeField]
    private LayerMask mouseRaycastLayer = (1 << 0);
    [SerializeField]
    private float fadeSeconds = 0.2f;
    [SerializeField]
    private float randomisation = 0.08f;

    private LineRenderer lineRenderer;
    private Transform target;
    private Material lineMaterial;
    private bool connected;

    private Camera mainCamera;

    private Color lineColor;
    private float currentAlpha;

    protected override void AwakeEx()
    {
        base.AwakeEx();

        mainCamera = Camera.main;

        GetComponentEx(ref lineRenderer);

        lineMaterial = lineRenderer.material;
        lineColor = lineMaterial.GetColor(MAT_PROPERTY);

        lineRenderer.enabled = false;
    }

    private void Update()
    {
        if (connected)
        {
            lineRenderer.enabled = true;
            currentAlpha = Mathf.Clamp01(currentAlpha + Time.deltaTime / fadeSeconds);
            lineColor.a = currentAlpha;
            lineMaterial.SetColor(MAT_PROPERTY, lineColor);
        }
        else
        {
            if (lineRenderer.enabled)
            {
                currentAlpha = Mathf.Clamp01(currentAlpha - Time.deltaTime / fadeSeconds);
                lineColor.a = currentAlpha;
                lineMaterial.SetColor(MAT_PROPERTY, lineColor);

                if (currentAlpha <= 0)
                    lineRenderer.enabled = false;
            }
        }

        if (lineRenderer.enabled)
        {
            Vector3 p1 = cachedTransform.position;

            Vector3 p2 = GetMouseWorldPosition();
            p2.y = target.position.y;
            p2 = Vector3.Lerp(p1, p2, 0.5f);

            Vector3 p3 = target.position;


            lineRenderer.positionCount = segments;
            float increment = 1.0f / (segments - 1);
            float t = 0;
            for (int i = 0; i < segments; i++)
            {
                Vector3 bezierPoint = Bezier(p1, p2, p3, t);
                bezierPoint += UnityEngine.Random.insideUnitSphere * randomisation * t;

                lineRenderer.SetPosition(i, bezierPoint);

                t += increment;
            }
        }
    }

    public void ConnectTo(Transform target)
    {
        if (target == null)
            return;

        this.target = target;
        connected = true;
    }

    public void Disconnect()
    {
        connected = false;
    }

    private Vector3 GetMouseWorldPosition()
    {
        Ray mouseRay = mainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if(Physics.Raycast(mouseRay, out hit, 1000, mouseRaycastLayer))
            return hit.point;

        return Vector3.zero;
    }

    private Vector3 Bezier(Vector3 p1, Vector3 p2, Vector3 p3, float t)
    {
        return Mathf.Pow(1 - t,2) *p1 + 2 * (1 - t) * t * p2 + Mathf.Pow(t,2) * p3;
    }
}
