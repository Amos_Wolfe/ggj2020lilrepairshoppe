﻿using MossWolf.Base;
using SellEffects;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum ItemMaterial
{
    Wood,
    Clay,
    Stone,
    Plastic,
    Squelchy,
    Metal,
}

/// <summary>
/// Information that is sent via an Item's OnSell unity event.
/// </summary>
[System.Serializable]
public class ItemSellInfo
{
    public Item Item;
}

public class Item : BaseBehaviour
{
    [System.Serializable]
    public class ItemSellEvent : UnityEvent<ItemSellInfo> { }

    private const float SCRAMBLE_DISTANCE = 3;
    private const float SCRAMBLE_HEIGHT = 0.5f;

    [SerializeField]
    private ItemMaterial itemMaterial;
    [SerializeField]
    private AttachmentPoint attachmentPointTemplate;

    [SerializeField]
    private List<AttachmentPoint> configuredAttachmentPoints;

    [SerializeField]
    private float minSellAmount = 5;
    [SerializeField]
    private float maxSellAmount = 100;
    [SerializeField]
    private float assemblePositionHeight = 1;
    [SerializeField]
    private float attachmentPointScale = 1;

    private List<SellEffectBase> sellEffectList;

    private List<Segment> segments;
    
    private Collider[] colliderList;

    public float MinSellAmount => minSellAmount;
    public float MaxSellAmount => maxSellAmount;

    public float AssemblePositionHeight => assemblePositionHeight;

    public Vector3 AveragePosition
    {
        get
        {
            if (segments.Count == 0)
                return transform.position;

            Vector3 averagePosition = Vector3.zero;
            for(int i = 0; i < segments.Count; i++)
            {
                Segment segment = segments[i];
                averagePosition += segment.transform.position;
            }
            averagePosition /= segments.Count;

            return averagePosition;
        }
    }

    public List<Segment> Segments => segments;

    public ItemMaterial ItemMaterial => itemMaterial;
    
    /// <summary>
    /// A value from 0 to 1, where 0 is completely unfixed and 1 is completly fixed
    /// </summary>
    public float RepairPercentage
    {
        get
        {
            if (segments.Count == 0)
                return 0;

            float segmentsRepaired = 0;

            for(int i = 0; i < segments.Count; i++)
            {
                Segment currentSegment = segments[i];
                segmentsRepaired += currentSegment.RepairPercentage;
            }

            return Mathf.Clamp01(segmentsRepaired / segments.Count);
        }
    }

    protected override void AwakeEx()
    {
        base.AwakeEx();

        sellEffectList = new List<SellEffectBase>(GetComponents<SellEffectBase>());

        colliderList = new Collider[10];

        segments = new List<Segment>(GetComponentsInChildren<Segment>());

        ScrambleSegments();
    }

    public void SellItem()
    {
        if (RepairPercentage < 0.9999f)
            return;

        StartCoroutine(_RunSellEffects());
    }

    public void DoPickupEffects()
    {
        StartCoroutine(_RunSellEffects());
    }

    private IEnumerator _RunSellEffects()
    {
        ItemSellInfo info = new ItemSellInfo
        {
            Item = this,
        };

        for (int i = 0; i < sellEffectList.Count; i++)
            yield return sellEffectList[i]._RunSellEffect(info);
    }
    
    private void ScrambleSegments()
    {
        foreach(Segment segment in segments)
        {
            Vector3 newPosition = segment.transform.position;
            newPosition.x += Random.Range(-SCRAMBLE_DISTANCE, SCRAMBLE_DISTANCE);
            newPosition.y += SCRAMBLE_HEIGHT;
            newPosition.z += Random.Range(-SCRAMBLE_DISTANCE, SCRAMBLE_DISTANCE);
            segment.transform.position = newPosition;

            segment.transform.rotation = Random.rotation;
        }
    }
}
