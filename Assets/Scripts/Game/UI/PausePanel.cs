﻿using MossWolf.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PausePanel : BaseBehaviour
{
    [SerializeField]
    private GameObject root;

    protected override void AwakeEx()
    {
        base.AwakeEx();

        Hide();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!root.activeSelf)
                Show();
            else
                Hide();
        }
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void Show()
    {
        if (!root.activeSelf)
            root.SetActive(true);
    }

    public void Hide()
    {
        if (root.activeSelf)
            root.SetActive(false);
    }
}