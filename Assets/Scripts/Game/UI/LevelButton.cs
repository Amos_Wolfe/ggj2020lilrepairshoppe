﻿using MossWolf.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LevelButton : BaseBehaviour
{
    [SerializeField]
    private Text levelText;
    [SerializeField]
    private GameObject buttonSelectedObject;

    public int Level
    {
        get => level;
        set
        {
            level = value;
            levelText.text = level.ToString();
        }
    }
    private int level;

    public UnityAction OnClick;

    private void OnDisable()
    {
        buttonSelectedObject.SetActive(false);
    }

    private void OnMouseUp()
    {
        OnClick?.Invoke();
    }
}
