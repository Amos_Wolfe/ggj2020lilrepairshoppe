﻿using MossWolf.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OpenSceneButton : BaseBehaviour
{
    private FadePanel fadePanel;
    private string targetScene;

    protected override void AwakeEx()
    {
        base.AwakeEx();

        ReferenceManager.GetBehaviours(ref fadePanel);

        fadePanel.OnFadeOut += OnFadeOut;
    }

    protected void OnDestroy()
    {
        fadePanel.OnFadeOut -= OnFadeOut;
    }

    public void Open(string sceneName)
    {
        if (fadePanel.Fading)
            return;

        this.targetScene = sceneName;
        fadePanel.FadeOut();
    }

    private void OnFadeOut()
    {
        if (string.IsNullOrEmpty(targetScene))
            return;

        ReferenceManager.ClearReferences();
        SceneManager.LoadScene(targetScene);
    }
}