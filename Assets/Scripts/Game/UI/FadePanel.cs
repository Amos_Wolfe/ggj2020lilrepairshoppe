﻿using MossWolf.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class FadePanel : BaseBehaviour
{
    public const float LOWEST_VOLUME = -80;

    public delegate void FadeEvent();

    [SerializeField]
    private float fadeSeconds = 1;
    [SerializeField]
    private CanvasGroup alphaGroup;
    [SerializeField]
    private bool fadeInOnStart = true;

    [SerializeField]
    private bool fadeOutSound = true;
    [SerializeField]
    private AudioMixer audioMixer;
    [SerializeField]
    private string volumeKey = "Volume";
    [SerializeField]
    private float targetVolume = -18;

    public event FadeEvent OnFadeIn;
    public event FadeEvent OnFadeOut;

    /// <summary>
    /// Set if this panel is in the process of fading in or out.
    /// </summary>
    public bool Fading { get; private set; }

    private void Start()
    {
        alphaGroup.gameObject.SetActive(true);
        FadeIn();
    }

    public void FadeIn()
    {
        StopAllCoroutines();
        StartCoroutine(_FadeIn());
    }

    public void FadeOut()
    {
        Time.timeScale = 1;
        StopAllCoroutines();
        StartCoroutine(_FadeOut());
    }

    private IEnumerator _FadeIn()
    {
        Fading = true;

        float t = 0;
        while(t < 1)
        {
            t = Mathf.Clamp01(t + Time.deltaTime / fadeSeconds);
            alphaGroup.alpha = 1 - t;

            if(fadeOutSound)
                audioMixer.SetFloat(volumeKey, Mathf.Lerp(LOWEST_VOLUME, targetVolume, t));

            yield return null;
        }

        alphaGroup.alpha = 0;
        yield return null;

        Fading = false;

        OnFadeIn?.Invoke();
    }

    public IEnumerator _FadeOut()
    {
        Fading = true;

        float t = 0;
        while (t < 1)
        {
            t = Mathf.Clamp01(t + Time.unscaledDeltaTime / fadeSeconds);
            alphaGroup.alpha = t;

            if(fadeOutSound)
                audioMixer.SetFloat(volumeKey, Mathf.Lerp(targetVolume, LOWEST_VOLUME, t));

            yield return null;
        }

        alphaGroup.alpha = 1;
        yield return null;

        Fading = false;

        OnFadeOut?.Invoke();
    }
}