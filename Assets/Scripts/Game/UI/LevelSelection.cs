﻿using MossWolf.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelection : BaseBehaviour
{
    [SerializeField]
    private LevelButton levelButtonTemplate;
    [SerializeField]
    private int columnCount = 4;
    [SerializeField]
    private float buttonSpacing = 2;

    private LevelManager levelManager;

    protected override void AwakeEx()
    {
        base.AwakeEx();

        levelButtonTemplate.gameObject.SetActive(false);

        ReferenceManager.GetBehaviours(ref levelManager);
    }

    private void Start()
    {
        ConfigureLevels();
    }

    private void ConfigureLevels()
    {
        int levelCount = levelManager.LevelCount;

        for(int i = 0; i < levelCount; i++)
        {
            int column = i % columnCount;
            int row = i / columnCount;

            LevelButton newButton = Instantiate(levelButtonTemplate, transform);
            newButton.transform.position = levelButtonTemplate.transform.position;
            newButton.transform.rotation = levelButtonTemplate.transform.rotation;
            newButton.transform.Translate(column * buttonSpacing, 0, -row * buttonSpacing);
            newButton.gameObject.SetActive(true);

            int level = i + 1;
            newButton.Level = level;
            newButton.OnClick += delegate
            {
                OnLevelSelected(level);
            };
        }
    }

    private void OnLevelSelected(int level)
    {
        levelManager.StartLevel(level);
        gameObject.SetActive(false);
    }
}
