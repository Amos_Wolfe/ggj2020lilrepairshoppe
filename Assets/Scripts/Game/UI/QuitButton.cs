﻿using MossWolf.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitButton : BaseBehaviour
{
    public void Quit()
    {
        Application.Quit();
    }
}