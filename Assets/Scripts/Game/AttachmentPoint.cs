﻿using MossWolf.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class AttachmentPoint : BaseBehaviour
{
    
    private const float LINE_WIDTH = 2;
    private const float GUI_ANIMATE_SPEED = 6;
    private const float MAX_DRAG_DISCONNECT = 16;
    private const float DROP_SHADOW_DISTANCE = 1;
    
    private const float ICON_MAX_ALPHA = 1;

    private string ATTACHMENT_POINT_LAYER = "AttachmentPoint";

    [SerializeField, Tooltip("The physical force applied when breaking an attachment point.")]
    private float detachForce = 20;
    
    [SerializeField, Tooltip("The frames per second in the icon animation.")]
    private float iconAnimationFPS = 6;
    [SerializeField, Tooltip("The size of the icon animation that appears when the cursor is hovering over an attachment point, in pixels")]
    private int iconAnimationSize = 25;

    [SerializeField]
    private GameObject unattachedObject;

    [SerializeField]
    private Texture2D[] attachmentIndicators;
    [SerializeField]
    private Texture2D[] detachmentIndicators;
    [SerializeField]
    private Texture2D attachmentLineTexture;
    [SerializeField]
    private Color attachHighlightColor = new Color(0, 1, 0, 0.2f);
    [SerializeField]
    private Color detachHighlightColor = new Color(1, 0, 0, 0.2f);

    [SerializeField]
    private Texture2D occludedTexture;
    [SerializeField]
    private Color occludedUnattachedColor;
    [SerializeField]
    private Color occludedAttachedColor;

    [SerializeField]
    private AttachmentPoint correctAttachment;

    private SegmentAttacher segmentAttacher;

    private Camera mainCamera;
    private Transform cameraTransform;
    private int attachmentLayer;
    private bool dragged;
    private bool hovered;
    private Vector2 mouseStart;
    private LineDrawer lineDrawer;

    private AttachmentPoint attachmentHoveringOver;

    public Segment ParentSegment { get; private set; }
    public FixedJoint Joint { get; set; }
    public AttachmentPoint CorrectAttachment => correctAttachment;

    public AttachmentPoint AttachedTo
    {
        get => attachedTo;
        set
        {
            attachedTo = value;
            unattachedObject.SetActive(attachedTo == null);
        }
    }
    private AttachmentPoint attachedTo;

    protected override void AwakeEx()
    {
        base.AwakeEx();

        ReferenceManager.GetBehaviours(ref segmentAttacher);

        lineDrawer = new LineDrawer(attachmentLineTexture);

        mainCamera = Camera.main;
        cameraTransform = mainCamera.transform;
        attachmentLayer = LayerMask.GetMask(ATTACHMENT_POINT_LAYER);
        ParentSegment = GetComponentInParent<Segment>();
    }

    private void OnGUI()
    {
        bool showIndicator = false;
        if (AttachedTo == null)
        {
            showIndicator = hovered || dragged;
        }
        else
        {
            if (Input.GetMouseButton(0))
                showIndicator = dragged;
            else
                showIndicator = hovered;
        }

        Vector3 position = cachedTransform.position;
        
        Vector3 screenPosition = mainCamera.WorldToScreenPoint(position);
        float mouseDistance = Vector2.Distance(Input.mousePosition, screenPosition);
        screenPosition.y = Screen.height - screenPosition.y;

        if (showIndicator)
        {
            Rect rect = GetSquareRect(screenPosition, iconAnimationSize);

            Texture2D highlightTexture = null;

            if (AttachedTo == null)
                highlightTexture = attachmentIndicators[(int)(Time.time * GUI_ANIMATE_SPEED) % attachmentIndicators.Length];
            else
                highlightTexture = detachmentIndicators[(int)(Time.time * GUI_ANIMATE_SPEED) % detachmentIndicators.Length];

            DrawDropShadowTexture(rect, highlightTexture, AttachedTo == null ? attachHighlightColor : detachHighlightColor);
        }

        // Draw Line
        if(dragged && AttachedTo == null)
        {
            Vector2 startScreenPos = mainCamera.WorldToScreenPoint(position);
            startScreenPos.y = Screen.height - startScreenPos.y;
            Vector2 endScreenPos = Input.mousePosition;

            if (attachmentHoveringOver != null)
                endScreenPos = mainCamera.WorldToScreenPoint(attachmentHoveringOver.transform.position);

            endScreenPos.y = Screen.height - endScreenPos.y;

            lineDrawer.Draw(startScreenPos, endScreenPos, LINE_WIDTH);
        }
    }

    private void Update()
    {
        hovered = false;

        Ray mouseRay = mainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(mouseRay, out hit, 1000, attachmentLayer, QueryTriggerInteraction.Collide))
        {
            if (hit.transform == cachedTransform)
                hovered = true;
            else
                attachmentHoveringOver = hit.transform.GetComponent<AttachmentPoint>();
        }
        else
            attachmentHoveringOver = null;

        if (dragged || hovered)
        {
            if (Input.GetMouseButtonDown(0))
            {
                dragged = true;
                OnBeginDrag();
            }
            else if(Input.GetMouseButtonUp(0))
            {
                if (dragged)
                {
                    dragged = false;
                    OnEndDrag();
                }
            }
            else if(Input.GetMouseButton(0))
            {
                if (dragged)
                    OnDrag();
            }
        }
    }

    private void OnBeginDrag()
    {
        mouseStart = Input.mousePosition;
    }

    private void OnDrag()
    {
        if(AttachedTo != null)
        {
            float mouseDistance = Vector2.Distance(Input.mousePosition, mouseStart);
            if (mouseDistance > MAX_DRAG_DISCONNECT)
                dragged = false;
        }
    }

    private void OnEndDrag()
    {
        if (segmentAttacher.Busy)
            return;
        if (AttachedTo != null)
        {
            if(hovered)
                TryToDetach();
            return;
        }

        Ray mouseRay = mainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(mouseRay, out hit, 1000, attachmentLayer, QueryTriggerInteraction.Collide))
        {
            var otherPoint = hit.transform.GetComponent<AttachmentPoint>();

            if (otherPoint != null)
                TryToAttach(otherPoint);
        }
    }

    public void TryToAttach(AttachmentPoint otherPoint)
    {
        if (segmentAttacher.Busy)
            return;

        if (otherPoint.ParentSegment == ParentSegment)
            // From the same segment, so don't attach
            return;

        if (!segmentAttacher.Attach(this, otherPoint))
            return;

        AttachedTo = otherPoint;
        otherPoint.AttachedTo = this;
    }

    private void TryToDetach()
    {
        if (segmentAttacher.Busy)
            return;

        if (AttachedTo == null)
            return;
        
        Segment attachedSegment = AttachedTo.ParentSegment;

        segmentAttacher.Detach(this, AttachedTo);

        attachedSegment.Rigidbody.AddExplosionForce(detachForce, transform.position, 10, 0, ForceMode.VelocityChange);
        ParentSegment.Rigidbody.AddExplosionForce(detachForce, transform.position, 10, 0, ForceMode.VelocityChange);
    }

    private void DrawDropShadowTexture(Rect rect, Texture2D texture, Color shadowColor, float alpha = 1)
    {
        rect.x += DROP_SHADOW_DISTANCE;
        rect.y += DROP_SHADOW_DISTANCE;
        GUI.color = new Color(0, 0, 0, 0.5f * alpha);
        GUI.DrawTexture(rect, texture);

        rect.x -= DROP_SHADOW_DISTANCE * 2;
        rect.y -= DROP_SHADOW_DISTANCE * 2;
        shadowColor.a *= alpha;
        GUI.color = shadowColor;
        GUI.DrawTexture(rect, texture);

        rect.x += DROP_SHADOW_DISTANCE;
        rect.y += DROP_SHADOW_DISTANCE;
        GUI.color = new Color(1,1,1, alpha);
        GUI.DrawTexture(rect, texture);
    }

    private Rect GetSquareRect(Vector2 screenPosition, float size)
    {
        return new Rect(screenPosition.x - size / 2, screenPosition.y - size / 2, size, size);
    }
}
