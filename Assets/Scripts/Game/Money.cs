﻿using MossWolf.Base;
using MossWolf.Base.Pooling;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Money : BaseBehaviour
{
    private const float EXPLODE_FORCE = 10;
    private const float EXPLODE_UP_FORCE = 5;

    [SerializeField]
    private float disappearMinSeconds = 1;
    [SerializeField]
    private float disappearMaxSeconds = 3;

    private float disappearSecondsLeft;

    private Rigidbody thisBody;

    public ComponentPool<Money> Pool { get; set; }

    private void OnEnable()
    {
        GetComponentEx(ref thisBody);

        disappearSecondsLeft = UnityEngine.Random.Range(disappearMinSeconds, disappearMaxSeconds);
    }

    private void Update()
    {
        if (disappearSecondsLeft <= 0)
            return;

        disappearSecondsLeft -= Time.deltaTime;

        if (disappearSecondsLeft <= 0)
            Disappear();
    }

    public void AddRandomForce()
    {
        thisBody.AddForce(UnityEngine.Random.onUnitSphere * EXPLODE_FORCE + Vector3.up * EXPLODE_UP_FORCE, ForceMode.VelocityChange);
    }

    private void Disappear()
    {
        Pool.Release(this);
    }
}
