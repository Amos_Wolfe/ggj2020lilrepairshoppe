using MossWolf.Base;
using Rewired;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCharacter : BaseBehaviour
{
    [SerializeField]
    private float moveSpeed = 2;
    [SerializeField]
    private float horizontalDampening = 20;
    [SerializeField]
    private float rotateSpeed = 10;
    [SerializeField]
    private string horizontalAxis = "Move Horizontal";
    [SerializeField]
    private string verticalAxis = "Move Vertical";

    [SerializeField]
    private string moveParameter = "Move";
    [SerializeField]
    private Animator animator;
    [SerializeField]
    private Transform pivot;

    private Player player;
    private Vector2 moveVector2D;
    private Vector3 moveVector3D;
    private float targetAngle;
    private float currentAngle;

    private Rigidbody body;

    protected override void AwakeEx()
    {
        base.AwakeEx();

        player = ReInput.players.GetPlayer(0);

        body = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        // Move
        moveVector2D = player.GetAxis2DRaw(horizontalAxis, verticalAxis);
        bool moving = moveVector2D.x != 0 || moveVector2D.y != 0;
        if(moving)
            moveVector2D.Normalize();

        moveVector3D.x = moveVector2D.x;
        moveVector3D.z = moveVector2D.y;

        Vector3 horizontalVelocity = body.velocity;
        horizontalVelocity.y = 0;
        body.AddForce(moveVector3D * moveSpeed - horizontalVelocity * horizontalDampening, ForceMode.Acceleration);

        // Pivot
        if(moving)
            targetAngle = Mathf.Atan2(-moveVector2D.y, moveVector2D.x) * Mathf.Rad2Deg;

        // Animate
        animator.SetBool(moveParameter, moving);
    }

    private void Update()
    {
        currentAngle = Mathf.LerpAngle(currentAngle, targetAngle, Time.deltaTime * rotateSpeed);
        pivot.localEulerAngles = new Vector3(0, currentAngle + 90, 0);
    }
}
