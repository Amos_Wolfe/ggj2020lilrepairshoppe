﻿using MossWolf.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelManager : BaseBehaviour
{
    [SerializeField]
    private GameObject sellButton;
    [SerializeField]
    private GameObject endLevelPanel;
    [SerializeField]
    private Text endLevelDescription;
                
    [SerializeField]
    private List<LevelConfig> levelConfigList;

    private LevelConfig activeLevel;

    private LevelSelection levelSelection;
    private ScoreManager scoreManager;
    private FadePanel fadePanel;
    private TutorialManager tutorialManager;

    private int itemGroupIndex = -1;
    private bool busy;
    private bool leaving;

    /// <summary>
    /// Returns the amount of levels there are to select.
    /// </summary>
    public int LevelCount => levelConfigList.Count;

    protected override void AwakeEx()
    {
        base.AwakeEx();

        endLevelPanel.SetActive(false);

        ReferenceManager.GetBehaviours(ref levelSelection, ref scoreManager, ref fadePanel);
        ReferenceManager.GetBehaviours(ref tutorialManager);
    }

    public void StartLevel(int level)
    {
        StartCoroutine(_StartLevel(level));
    }

    private IEnumerator _StartLevel(int level)
    {
        int levelIndex = level - 1;
        if (levelIndex < 0 || levelIndex >= levelConfigList.Count)
        {
            LogError("Level {0} is out of range", level);
            yield break;
        }
        
        sellButton.SetActive(true);
        activeLevel = levelConfigList[levelIndex];
        itemGroupIndex = -1;
        NextItemGroup(0.2f);

        if (level == 1)
        {
            yield return new WaitForSeconds(1);
            tutorialManager.StartTutorial();
        }
    }

    public void NextItemGroup(float waitTime)
    {
        if (busy)
            return;

        StartCoroutine(_NextItemGroup(waitTime));
    }

    public void BackToLevelSelection()
    {
        itemGroupIndex = -1;
        sellButton.SetActive(false);
        endLevelPanel.SetActive(false);
        levelSelection.gameObject.SetActive(true);
        scoreManager.ResetScore();
    }

    private IEnumerator _NextItemGroup(float waitTime)
    {
        busy = true;

        if (waitTime != 0)
            yield return new WaitForSeconds(waitTime);

        itemGroupIndex++;

        if (itemGroupIndex < activeLevel.ItemGroupList.Count)
            SpawnNewItems(activeLevel.ItemGroupList[itemGroupIndex]);
        else
            ShowEndScreen();

        busy = false;
    }

    private void SpawnNewItems(ItemGroup itemGroup)
    {
        for(int i = 0; i < itemGroup.ItemList.Count; i++)
        {
            Item itemTemplate = itemGroup.ItemList[i];
            Item newItem = Instantiate(itemTemplate, Vector3.up * 100 * i, Quaternion.identity);
        }
    }

    private void ShowEndScreen()
    {
        sellButton.SetActive(false);
        endLevelPanel.SetActive(true);
        endLevelDescription.text = string.Format("You earned ${0:0.00}", scoreManager.Score);
    }
}
