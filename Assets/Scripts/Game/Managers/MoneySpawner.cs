﻿using MossWolf.Base;
using MossWolf.Base.Pooling;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MoneyType
{
    Coin = 0,
    Note,
    Small,
    Large,
    Gold,
}

[System.Serializable]
public class MoneyTemplateInfo
{
    public MoneyType MoneyType;
    public Money Template;

    public ComponentPool<Money> Pool { get; set; }
}

public class MoneySpawner : BaseBehaviour
{
    [SerializeField]
    private List<MoneyTemplateInfo> moneyList;

    [SerializeField]
    private int initalPoolCount = 30;

    protected override void AwakeEx()
    {
        base.AwakeEx();

        for(int i = 0; i < moneyList.Count; i++)
        {
            MoneyTemplateInfo info = moneyList[i];
            info.Pool = new ComponentPool<Money>(info.Template, initalPoolCount);
        }
    }

    public void SpawnMoney(MoneyType moneyType, Vector3 position)
    {
        for(int i = 0; i < moneyList.Count; i++)
        {
            MoneyTemplateInfo info = moneyList[i];
            if(info.MoneyType == moneyType)
            {
                Money newMoney = info.Pool.Fetch();
                newMoney.Pool = info.Pool;
                newMoney.AddRandomForce();
                newMoney.transform.SetPositionAndRotation(position, UnityEngine.Random.rotation);
            }
        }
    }
}
