﻿using MossWolf.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialManager : BaseBehaviour
{
    private const float DELAY_BETWEEN_STEPS = 0.75f;
    private const float LINE_WIDTH = 2;
    private const int CURSOR_TEXTURE_SIZE = 32;
    private const int CLICK_TEXTURE_SIZE = 32;
    private const float ANIMATION_TIME = 1.5f;
    private const float CURSOR_PAUSE_PERCENT = 0.5f;

    private const string SELL_BUTTON_TAG = "SellButton";

    public enum Mode
    {
        None = 0,
        WaitingForItem,
        ShowingIncorrectConnection,
        ShowingDisconnection,
        ShowingCorrectConnectionOne,
        ShowingCorrectConnectionTwo,
        ShowingSellButton,
    }

    [SerializeField]
    private Texture2D cursorImage;
    [SerializeField]
    private Texture2D clickImage;
    [SerializeField]
    private Texture2D lineImage;

    private LineDrawer lineDrawer;
    private Mode mode;
    private Item item;
    private AttachmentPoint attachmentPoint1;
    private AttachmentPoint attachmentPoint2;
    private float timeOffset;
    private float displayDelayLeft;

    private GameObject sellButton;

    protected override void AwakeEx()
    {
        base.AwakeEx();

        mode = Mode.None;
        lineDrawer = new LineDrawer(lineImage);
    }

    private void Update()
    {
        if (mode == Mode.None)
            return;

        if(displayDelayLeft > 0)
        {
            displayDelayLeft -= Time.deltaTime;
            return;
        }

        if (!sellButton.gameObject.activeSelf)
        {
            mode = Mode.None;
            return;
        }

        switch (mode)
        {
            case Mode.WaitingForItem:
                UpdateWaitingForItem();
                break;

            case Mode.ShowingIncorrectConnection:
                UpdateShowingConnection();
                break;

            case Mode.ShowingDisconnection:
                UpdateShowingDisconnection();
                break;

            case Mode.ShowingCorrectConnectionOne:
                UpdateShowingCorrectConnection();
                break;

            case Mode.ShowingCorrectConnectionTwo:
                UpdateShowingCorrectConnectionTwo();
                break;
        }
    }

    private void OnGUI()
    {
        if (mode == Mode.None)
            return;
        if (displayDelayLeft > 0)
            return;

        if (mode == Mode.ShowingIncorrectConnection || mode == Mode.ShowingCorrectConnectionOne || mode == Mode.ShowingCorrectConnectionTwo)
        {
            Camera mainCamera = Camera.main;

            Vector3 position1 = mainCamera.WorldToScreenPoint(attachmentPoint1.transform.position);
            position1.y = Screen.height - position1.y;
            Vector3 position2 = mainCamera.WorldToScreenPoint(attachmentPoint2.transform.position);
            position2.y = Screen.height - position2.y;

            DrawMovingCursor(position1, position2, true, true);
        }
        else if(mode == Mode.ShowingDisconnection)
        {
            Camera mainCamera = Camera.main;

            Vector3 position1 = mainCamera.WorldToScreenPoint(attachmentPoint1.transform.position);
            position1.y = Screen.height - position1.y;
            DrawClickingCursor(position1);
        }
        else if(mode == Mode.ShowingSellButton)
        {
            Camera mainCamera = Camera.main;

            if(sellButton != null)
            {
                Vector2 sellButtonPos = mainCamera.WorldToScreenPoint(sellButton.transform.position);
                sellButtonPos.y = Screen.height - sellButtonPos.y;

                Vector2 firstPos = sellButtonPos + new Vector2(100,200);

                DrawMovingCursor(firstPos, sellButtonPos, false, false);
            }
        }
    }

    /// <summary>
    /// Starts the tutorial, showing the player how to connect and detach segments.
    /// </summary>
    public void StartTutorial()
    {
        mode = Mode.WaitingForItem;
        sellButton = GameObject.FindGameObjectWithTag(SELL_BUTTON_TAG);
        timeOffset = Time.time + DELAY_BETWEEN_STEPS + CURSOR_PAUSE_PERCENT;
        displayDelayLeft = DELAY_BETWEEN_STEPS;
    }

    private void UpdateWaitingForItem()
    {
        item = GameObject.FindObjectOfType<Item>();
        if (item != null)
        {
            mode = Mode.ShowingIncorrectConnection;
            OnNextStep();

            attachmentPoint1 = item.Segments[0].AttachmentPoints[0];
            attachmentPoint2 = item.Segments[1].AttachmentPoints[0];
        }
    }

    private void UpdateShowingConnection()
    {
        if (attachmentPoint1.AttachedTo != null)
        {
            mode = Mode.ShowingDisconnection;
            OnNextStep();
        }
    }

    private void UpdateShowingDisconnection()
    {
        if (attachmentPoint1.AttachedTo == null)
        {
            mode = Mode.ShowingCorrectConnectionOne;
            OnNextStep();

            attachmentPoint2 = attachmentPoint1.CorrectAttachment;
        }
    }

    private void UpdateShowingCorrectConnection()
    {
        if (attachmentPoint1.AttachedTo != null)
        {
            mode = Mode.ShowingCorrectConnectionTwo;
            OnNextStep();

            attachmentPoint1 = item.Segments[1].AttachmentPoints[0];
            attachmentPoint2 = item.Segments[1].AttachmentPoints[0].CorrectAttachment;
        }
    }

    private void UpdateShowingCorrectConnectionTwo()
    {
        if (attachmentPoint1.AttachedTo != null)
        {
            mode = Mode.ShowingSellButton;
            OnNextStep();
        }
    }

    private void OnNextStep()
    {
        displayDelayLeft = DELAY_BETWEEN_STEPS;
        timeOffset = Time.time + DELAY_BETWEEN_STEPS + CURSOR_PAUSE_PERCENT;
    }

    private void DrawClickingCursor(Vector2 screenPosition)
    {
        bool showClick = Time.time % 1 > 0.5f;

        if (showClick)
        {
            Rect clickRect = new Rect(screenPosition.x - CLICK_TEXTURE_SIZE / 2, screenPosition.y - CLICK_TEXTURE_SIZE / 2, CLICK_TEXTURE_SIZE, CLICK_TEXTURE_SIZE);
            GUI.DrawTexture(clickRect, clickImage);
        }

        Rect cursorRect = new Rect(screenPosition.x, screenPosition.y, CURSOR_TEXTURE_SIZE, CURSOR_TEXTURE_SIZE);
        GUI.DrawTexture(cursorRect, cursorImage);
    }

    private void DrawMovingCursor(Vector3 firstScreenPos, Vector2 secondScreenPos, bool displayClicking, bool drawLine)
    {
        float t = ((Time.time - timeOffset) % ANIMATION_TIME) / ANIMATION_TIME;
        t = Mathf.Clamp01((t * (1 + CURSOR_PAUSE_PERCENT * 2)) - CURSOR_PAUSE_PERCENT);

        Vector2 cursorPosition = Vector3.Lerp(firstScreenPos, secondScreenPos, t);

        if (displayClicking && t != 0 && t != 1)
        {
            Rect clickRect = new Rect(cursorPosition.x - CLICK_TEXTURE_SIZE / 2, cursorPosition.y - CLICK_TEXTURE_SIZE / 2, CLICK_TEXTURE_SIZE, CLICK_TEXTURE_SIZE);
            GUI.DrawTexture(clickRect, clickImage);
        }

        Rect cursorRect = new Rect(cursorPosition.x, cursorPosition.y, CURSOR_TEXTURE_SIZE, CURSOR_TEXTURE_SIZE);
        GUI.DrawTexture(cursorRect, cursorImage);

        if(drawLine)
            lineDrawer.Draw(firstScreenPos, cursorPosition, LINE_WIDTH);
    }
}
