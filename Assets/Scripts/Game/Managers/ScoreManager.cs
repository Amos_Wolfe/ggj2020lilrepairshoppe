﻿using MossWolf.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : BaseBehaviour
{
    private const float DELAY_BETWEEN_ITEMS = 0.3f;

    private const float GOLD_BAR_SCORE = 1000;
    private const float LARGE_SCORE = 100;
    private const float SMALL_SCORE = 10;
    private const float NOTE_SCORE = 1;
    private const float COIN_SCORE = 0.1f;

    [SerializeField]
    private Text scoreText;

    private bool selling;

    private MoneySpawner moneySpawner;
    private FloatingTextPanel floatingTextPanel;
    private LevelManager levelManager;

    private AudioSource cashRegisterAudio;

    private float score;

    public float Score => score;

    /// <summary>
    /// If set to true, this score manager will wait until this is set to false again to resume selling items.
    /// </summary>
    public bool PauseSellingItems { get; set; }

    protected override void AwakeEx()
    {
        base.AwakeEx();

        ReferenceManager.GetBehaviours(ref floatingTextPanel, ref moneySpawner, ref levelManager);

        GetComponentEx(ref cashRegisterAudio);
    }

    public void SellItems()
    {
        if (selling)
            return;
        StartCoroutine(_SellItems());
    }

    public void ResetScore()
    {
        score = 0;
        UpdateScoreText();
    }

    private IEnumerator _SellItems()
    {
        selling = true;
        Item[] items = GameObject.FindObjectsOfType<Item>();
        foreach (Item item in items)
        {
            item.SellItem();

            while (PauseSellingItems)
                yield return null;

            float repairPercent = item.RepairPercentage;
            float sellAmount = Mathf.Lerp(item.MinSellAmount, item.MaxSellAmount, repairPercent);
            sellAmount = Mathf.Round(sellAmount * 100) / 100.0f;
            score += sellAmount;
            
            floatingTextPanel.ShowFloatingText(string.Format("${0:0.00}", sellAmount), item.AveragePosition);
            
            if (cashRegisterAudio != null)
                cashRegisterAudio.Play();

            SpawnMoney(item, sellAmount);
            
            Destroy(item.gameObject);
            UpdateScoreText();
            yield return new WaitForSeconds(DELAY_BETWEEN_ITEMS);
        }

        UpdateScoreText();
        selling = false;

        levelManager.NextItemGroup(1);
    }

    public void AddScore(float amount, Vector3 worldPosition)
    {
        score += amount;

        floatingTextPanel.ShowFloatingText(string.Format("${0:0.00}", amount), worldPosition);

        if (cashRegisterAudio != null)
            cashRegisterAudio.Play();

        UpdateScoreText();
    }

    private void UpdateScoreText()
    {
        scoreText.text = string.Format("${0:0.00}", score);
    }

    private void SpawnMoney(Item item, float sellAmount)
    {
        List<Segment> segmentList = item.Segments;

        int maxIterations = 100;
        int iterations = 0;
        while(sellAmount > 0)
        {
            Transform randomSegment = segmentList[UnityEngine.Random.Range(0, segmentList.Count - 1)].transform;
            Vector3 position = randomSegment.position;

            if (sellAmount > GOLD_BAR_SCORE)
            {
                sellAmount -= GOLD_BAR_SCORE;
                moneySpawner.SpawnMoney(MoneyType.Gold, position);
            }
            else if(sellAmount > LARGE_SCORE)
            {
                sellAmount -= LARGE_SCORE;
                moneySpawner.SpawnMoney(MoneyType.Large, position);
            }
            else if(sellAmount > SMALL_SCORE)
            {
                sellAmount -= SMALL_SCORE;
                moneySpawner.SpawnMoney(MoneyType.Small, position);
            }
            else if(sellAmount > NOTE_SCORE)
            {
                sellAmount -= NOTE_SCORE;
                moneySpawner.SpawnMoney(MoneyType.Note, position);
            }
            else
            {
                sellAmount -= COIN_SCORE;
                moneySpawner.SpawnMoney(MoneyType.Coin, position);
            }

            iterations++;
        }
    }
}
