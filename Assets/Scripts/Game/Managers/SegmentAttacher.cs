﻿using MossWolf.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ItemMaterialSounds
{
    public ItemMaterial ItemMaterial;
    public List<AudioClip> Sounds;
}

/// <summary>
/// When the player indicates 2 attachment points to be connected, this class automatically moves those pieces so they connect.
/// </summary>
public class SegmentAttacher : BaseBehaviour
{
    private const float ATTACHMENT_TIME = 0.05f;

    private class MoveObjectInfo
    {
        public Rigidbody Rigidbody { get; set; }
        
        public Vector3 StartPosition { get; set; }
        public Quaternion StartRotation { get; set; }

        public Vector3 TargetPosition { get; set; }
        public Quaternion TargetRotation { get; set; }

        public MoveObjectInfo(Rigidbody rigidbody, Vector3 targetPosition, Quaternion targetRotation)
        {
            this.Rigidbody = rigidbody;
            this.TargetPosition = targetPosition;
            this.TargetRotation = targetRotation;
            this.StartPosition = rigidbody.position;
            this.StartRotation = rigidbody.rotation;
        }

        public void Move(float t)
        {
            Rigidbody.MovePosition(Vector3.Lerp(StartPosition, TargetPosition, t));
            Rigidbody.MoveRotation(Quaternion.Slerp(StartRotation, TargetRotation, t));
        }

        public void FinishMoving()
        {
            Rigidbody.MovePosition(TargetPosition);
            Rigidbody.MoveRotation(TargetRotation);
        }
    }

    [SerializeField]
    private List<ItemMaterialSounds> itemMaterialSounds;

    private AudioSource thisAudio;

    public bool Busy { get; private set; }

    protected override void AwakeEx()
    {
        base.AwakeEx();

        GetComponentEx(ref thisAudio);
    }

    public bool Attach(AttachmentPoint firstPoint, AttachmentPoint secondPoint)
    {
        // Cannot attach if one of the points is already attached to something
        if (firstPoint.AttachedTo != null || secondPoint.AttachedTo != null)
            return false;

        List<Segment> segmentList = FindAllAttachedSegments(firstPoint.ParentSegment);
        if (segmentList.Contains(secondPoint.ParentSegment))
            // Already connected through other connections
            return false;

        StartCoroutine(_Attach(firstPoint,secondPoint));
        return true;
    }

    public void Detach(AttachmentPoint firstPoint, AttachmentPoint secondPoint)
    {
        if (firstPoint.Joint != null)
        {
            GameObject.Destroy(firstPoint.Joint);
            firstPoint.Joint = null;
        }

        if (secondPoint.Joint != null)
        {
            GameObject.Destroy(secondPoint.Joint);
            secondPoint.Joint = null;
        }

        // Reset ignore collisions
        List<Segment> segmentList = FindAllAttachedSegments(firstPoint.ParentSegment);
        IgnoreCollisions(segmentList, false);

        firstPoint.AttachedTo = null;
        secondPoint.AttachedTo = null;

        // Set ignore collisions for each point
        segmentList = FindAllAttachedSegments(firstPoint.ParentSegment);
        IgnoreCollisions(segmentList, true);
        segmentList = FindAllAttachedSegments(secondPoint.ParentSegment);
        IgnoreCollisions(segmentList, true);
    }

    private IEnumerator _Attach(AttachmentPoint firstPoint, AttachmentPoint secondPoint)
    {
        Busy = true;

        firstPoint.ParentSegment.Rigidbody.isKinematic = true;
        secondPoint.ParentSegment.Rigidbody.isKinematic = true;

        Vector3 assemblePosition = firstPoint.CachedTransform.position;
        //assemblePosition.y += secondPoint.ParentSegment.ParentItem.AssemblePositionHeight;

        Quaternion startingRotation = firstPoint.ParentSegment.CachedTransform.rotation;

        // Move the first segment to an area where it won't get in the way
        Vector3 firstTargetPos = assemblePosition;
        Quaternion firstTargetRot = Quaternion.Inverse(firstPoint.CachedTransform.localRotation);
        
        // Move the second segment to connect to the first segment
        Vector3 secondTargetPos = firstTargetPos;
        Quaternion secondTargetRot = Quaternion.Euler(180, 0, 0) * Quaternion.Inverse(secondPoint.CachedTransform.localRotation);

        secondTargetRot = Quaternion.Inverse(firstTargetRot) * secondTargetRot;
        firstTargetRot = Quaternion.identity;

        firstTargetRot = startingRotation;
        secondTargetRot = startingRotation * secondTargetRot;

        Vector3 firstAttachmentOffset = firstTargetRot * firstPoint.CachedTransform.localPosition;
        Vector3 secondAttachmentOffset = secondTargetRot * secondPoint.CachedTransform.localPosition;

        firstTargetPos -= firstAttachmentOffset * firstPoint.CachedTransform.lossyScale.x;
        secondTargetPos -= secondAttachmentOffset * secondPoint.CachedTransform.lossyScale.x;

        yield return _Move(ATTACHMENT_TIME, 
            new MoveObjectInfo(firstPoint.ParentSegment.Rigidbody, firstTargetPos, firstTargetRot),
            new MoveObjectInfo(secondPoint.ParentSegment.Rigidbody, secondTargetPos, secondTargetRot));

        // Offset the second piece to attach properly
        Vector3 offset = secondPoint.CachedTransform.position - firstPoint.CachedTransform.position;
        secondTargetPos -= offset;

        yield return _Move(0, new MoveObjectInfo(secondPoint.ParentSegment.Rigidbody, secondTargetPos, secondTargetRot));

        PlaySound(firstPoint.ParentSegment.ParentItem.ItemMaterial, firstPoint.CachedTransform.position);

        // Attach the two segments together
        firstPoint.Joint = firstPoint.ParentSegment.gameObject.AddComponent<FixedJoint>();
        firstPoint.Joint.connectedBody = secondPoint.ParentSegment.Rigidbody;

        yield return new WaitForFixedUpdate();

        // Ignore collisions within the new segment group
        List<Segment> segmentList = FindAllAttachedSegments(firstPoint.ParentSegment);
        IgnoreCollisions(segmentList, true);

        Vector3 finalOffset = firstPoint.CachedTransform.position - secondPoint.CachedTransform.position;
        secondPoint.ParentSegment.CachedTransform.position += finalOffset;

        firstPoint.ParentSegment.Rigidbody.isKinematic = false;
        secondPoint.ParentSegment.Rigidbody.isKinematic = false;

        Busy = false;
    }

    private IEnumerator _Move(float time, params MoveObjectInfo[] objectsToMove)
    {
        float t = 0;
        while(t < 1)
        {
            t = Mathf.Clamp01(t + Time.deltaTime / time);

            foreach(MoveObjectInfo objectInfo in objectsToMove)
                objectInfo.Move(t);
            
            yield return new WaitForFixedUpdate();
        }

        foreach (MoveObjectInfo objectInfo in objectsToMove)
            objectInfo.FinishMoving();
    }

    private void IgnoreCollisions(Segment segment1, Segment segment2, bool ignoreCollisions)
    {
        if(segment1 == segment2)
        {
            LogError("Segment {0} cannot ignore collisions with itself", segment1);
            return;
        }

        foreach(Collider collider1 in segment1.Colliders)
        {
            foreach(Collider collider2 in segment2.Colliders)
            {
                Physics.IgnoreCollision(collider1, collider2, ignoreCollisions);
            }
        }
    }

    private void IgnoreCollisions(List<Segment> segmentList, bool ignoreCollisions)
    {
        for(int firstIndex = 0; firstIndex < segmentList.Count; firstIndex++)
        {
            int start = firstIndex + 1;
            for(int secondIndex = start; secondIndex < segmentList.Count; secondIndex++)
            {
                IgnoreCollisions(segmentList[firstIndex], segmentList[secondIndex], ignoreCollisions);
            }
        }
    }

    private List<Segment> FindAllAttachedSegments(Segment firstSegment)
    {
        List<Segment> closedList = new List<Segment>();
        List<Segment> openList = new List<Segment>();

        openList.Add(firstSegment);

        int iterations = 0;
        int maxIterations = 100;
        while(openList.Count != 0 && iterations < maxIterations)
        {
            Segment nextSegment = openList[0];
            openList.RemoveAt(0);
            closedList.Add(nextSegment);

            foreach(AttachmentPoint point in nextSegment.AttachmentPoints)
            {
                if(point.AttachedTo != null)
                {
                    Segment newSegment = point.AttachedTo.ParentSegment;
                    if (!closedList.Contains(newSegment) && !openList.Contains(newSegment))
                        openList.Add(newSegment);
                }
            }

            iterations++;
        }

        return closedList;
    }

    private void PlaySound(ItemMaterial itemMaterial, Vector3 position)
    {
        cachedTransform.position = position;
        
        foreach(ItemMaterialSounds materialSounds in itemMaterialSounds)
        {
            if(materialSounds.ItemMaterial == itemMaterial && materialSounds.Sounds.Count != 0)
            {
                AudioClip clip = materialSounds.Sounds[UnityEngine.Random.Range(0,materialSounds.Sounds.Count)];
                thisAudio.PlayOneShot(clip);
                return;
            }
        }
    }
}
