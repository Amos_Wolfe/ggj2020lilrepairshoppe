﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Contains a list of items.
/// </summary>
[System.Serializable]
public class ItemGroup
{
    public List<Item> ItemList;
}

[CreateAssetMenu(fileName = "Level",menuName = "Config/Level", order = 1)]
public class LevelConfig : ScriptableObject
{
    public List<ItemGroup> ItemGroupList;
}
